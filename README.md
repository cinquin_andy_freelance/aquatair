Logiciel Aquatair gestion

# Présentation du logiciel

Le logiciel se présente donc ainsi,

Nous avons une barre de navigation qui sera toujours présente peu importe l’endroit dans
l’application, dans cette barre de navigations nous avons les éléments suivants :

```
Le bouton d’accueil avec le logo de l’entreprise :
Qui permettra de revenir à l’accueil depuis n’importe quelle page.
```
```
Une icône pour chaque ‘catégories’ d’éléments, nous reviendrons dessus, mais dans l’ordre :
les Personnes
Les Chantiers
Les Catégories
Les équipements
Les affectations
```
```
Un bouton pour télécharger le contenu de la base de données sous format CSV
```
```
Un bouton de déconnexion
```

# Page d’accueil

Dans la page d’accueil nous avons donc 5 catégories principales :

- Personnes
- Chantiers
- Catégories
- Équipements
- Affectations

Toutes les icônes sont cliquables pour accéder à la liste correspondante.

La liste pour chaque élément permet la modification des données, la recherche, le tri des éléments,
la suppression d’un élément, et l’accès aux statistiques / historique par rapport à cet élément.

Nous pouvons également accéder avec le bouton prévu à cet effet à l’ajout d’éléments et de donnée.

## Onglet de gestion – Présentation générale


Chaque page de gestions se présente de la même façon, un tableau représentant les données, avec
comme possibilité :

- La recherche avec le champ prévu à cet effet, la barre de recherche est capable de chercher
  dans tous les champs affichés principalement à l’écran,
  Vous pouvez donc chercher par nom, prénom, numéro de téléphone, par référence, ou
  même par date
- Vous pouvez ensuite sélectionner plusieurs éléments à la fois afin de les supprimer de la base
  de données plus rapidement (attention! cette action est irréversible, et vous perdrez les
  historiques et traces par rapport aux éléments supprimés)
- Vous pouvez également trier les éléments par ordre alphabétique ou par numéro, ou bien
  par dates, en cliquant sur le nom de la colonne que vous voulez classer.
- Sur chacune des pages de gestions, un bouton de suppressions de groupe est présent en
  haut à droite de la page, il permet de gérer les suppressions de masses et de supprimer des
  éléments de manière plus pratique et rapide. (Attention! cette action est irréversible, et
  vous perdrez les historiques et traces par rapport aux éléments supprimés)
- Vous pouvez aussi naviguer vers les pages d’ajouts d’éléments qui correspondent à l’élément
  où vous êtes présent. Par exemple si vous êtes sur la liste des personnes, le bouton d’ajout
  vous emmènera vers la page d’ajout des personnes.
- Vous pouvez accéder à la page de statistique avec le petit logo bleu (petites barres de
  statistiques). Présent sur chacune des lignes, nous reviendrons dessus.
- Supprimé un élément de manière unique (attention! cette action est irréversible, et vous
  perdrez les historiques et traces par rapport à l’élément supprimé)
- Enfin en cliquant sur la petite flèche à côté d’une ligne, vous déplierez les informations
  correspondantes à la ligne dépliée, vous pourrez alors mettre à jours les données (attention,
  pour que la validation soit prise en compte vous devez cliquer sur le bouton « mettre à
  jour »), les informations dépliées seront différentes suivant sur quelle catégorie d’éléments
  vous vous trouvez.

## Onglet de gestion – Personnes

La gestion des personnes permet donc la modification des personnes, la recherche des personnes, et
l’ajout de nouvelles personnes, les ‘personnes’ peuvent être utilisées comme étant des acteurs.


Et n’importe quel nom peut être mis dans les champs, même chose pour les numéros de téléphone,
ils ne sont pas obligatoirement français.

## Onglet de gestion – Chantiers

La gestion des chantiers permet donc la modification des chantiers, la recherche des chantiers, et
l’ajout de nouveaux chantiers. Un chantier possède un nom, une ville, une adresse, et on peut lui
mettre une description, ce champ peut être utilisé comme moyen de ‘note’ pour se souvenir de
quelque chose, ou d’informations complémentaires.

## Onglet de gestion – Catégories

La gestion des catégories permet donc la modification des catégories, la recherche des catégories, et
l’ajout de nouvelles catégories.

Vous pouvez mettre une référence pour une catégorie, les catégories, ont été réfléchis pour être
utilisées comme étant des sortes de ‘dossiers’ permettant de ranger plus efficacement les
équipements entre eux dans le logiciel,


À la base nous l’avions pensé de façon à ce qu’il soit utilisé comme moyen de rangement, de telle
sorte à ce qu’il y ait une catégorie ‘Ballon d’eau chaude’, et que tous les équipements étant des
ballons d’eau chaude, quelque soit leurs fonctionnements appartiennent à cette même catégorie,
Dans leurs références apparaitra alors, au début, la référence indiquée pour la catégorie,
Par exemple, nous avons 4 ballons d’eau chaude, de 4 marques et 4 fonctionnements différents
Nous créons une catégorie ‘Ballon d’eau chaude’, avec comme référence BLN, et mettons comme
catégorie assignée à chaque ballon d’eau chaude cette même catégorie.
Chaque référence d’équipement aura alors en tant que préfixe la référence de la catégorie en
question, ce qui permettra un classement plus efficace et une recherche plus efficiente.

Après rien n’empêche une autre utilisation comme des classements plus généraux des équipements,
cela reste à voir l’utilisation du logiciel.

## Onglet de gestion – Équipements

La gestion des équipements permet donc la modification des équipements, la recherche des
équipements, et l’ajout de nouveaux équipements.

Les équipements possèdent une référence lisible qui permette de les identifier rapidement, le champ
de référence est libre et vous pouvez y mettre la notation de votre choix, que ça soit avec des lettres
ou des chiffres, nous vous conseillons néanmoins d’utiliser une notation logique avec des chiffres si
vous utilisez les catégories comme prévu initialement.

Les équipements peuvent possèdes également beaucoup de champ personnalisable aux besoins et
utilisable comme bon vous semble, le type étant prévu comme une petite description du genre
l’équipement, par exemple on mettra ‘électrique’ / ‘manuel’, ou ce genre d’information à l’intérieur
de ce champ, et enfin la description qui peux être utilisé également comme bon vous sembles.

On peut également lier un équipement à une catégorie en sélectionnant la catégorie en question (en
cliquant sur le champ), puis en validant le changement en cliquant sur ‘mettre à jour’.


## Onglet de gestion – Affectations

La gestion des affectations permet donc la modification des affectations, la recherche des
affectations, et l’ajout de nouvelles affectations.

C’est cet élément qui représentera le fait qu’un équipement est présent à tel chantier ou avec telle
personne, c’est avec cet élément également que vous pourrez trouver le plus d’informations liées à
la disponibilité, la date de récupération, de rendues ...

Vous pouvez sélectionner seulement une personne ou un chantier, pas les deux, et une date de
début & de fin d’affectation.

# Ajout de donnée

Chaque élément, que ça soit équipements, catégories, personnes, affectations, ou chantiers, possède
une page d’ajout, cette page d’ajout est accessible par l’accueil ou par les pages des éléments en
question en appuyant sur le bouton ‘ajouter’.

## Ajout de personne

Il faut pour une personne spécifier obligatoirement le nom, le prénom, et le numéro de téléphone


## Ajout de chantier

Pour un chantier, le nom et la ville sont obligatoires,
Pour la description et l’adresse, ce sont des éléments facultatifs et vous pourrez y mettre n’importe
quoi.

## Ajout de catégorie

Pour les catégories, une référence et un nom sont obligatoires, la description est facultative

## Ajout d’un équipement

Pour un équipement, la référence, le nom, et la date d’obtention sont des champs obligatoires, tout
le reste est facultatif, et vous pouvez y mettre ce que vous voulez.

Le type représente le ‘genre’ d’équipement, s’il est manuel, électrique, ou même la puissance de ce
dernier.

L’usure va représenter à quel point l’équipement est abimé.

La description permettra de prendre des notes par rapport à cet équipement, des informations
complémentaires si besoin.

Et vous pouvez assigner une catégorie à cet équipement, il prendra ainsi comme préfixe la référence
de la catégorie.


## Ajout d’assignations

La page principale de l’application, cette dernière va vous permettre de créer des assignations
rapidement et de rentrer toutes les données nécessaires rapidement,

ainsi vous pouvez cliquer sur l’onglet équipement afin de voir la liste des équipements apparaitre,
vous pouvez également rechercher manuellement (en tapant des caractères), pour trouver plus
facilement votre équipement, le plus efficace étant la recherche par référence.

Vous pouvez sélectionner plusieurs équipements à assigner à la même personne et au même
chantier en même temps, avec la même date, dans le même principe


# Statistiques

Chaque page de statistiques correspond aux statistiques propres à une donnée, suivant la page où
l’on se trouve.

Les pages de statistiques sont accessibles avec le petit bouton bleu sur les données, le petit logo de
statistiques

## Statistiques - Personnes

Lorsqu’on accède aux statistiques d’une personne, on récupère alors la liste de tout ce qu’il s’est
passé avec cette personne, tous les équipements qu’il a récupérés et toutes les interactions qu’il a
eues avec ces derniers.


## Statistiques - Chantiers

Lorsqu’on accède aux statistiques d’un chantier, on récupère alors la liste de tout ce qu’il s’est passé
avec ce chantier et tous les équipements qui y ont été assignés.

## Statistiques – Catégorie

On récupère la liste des tous les équipements liés à une catégorie en particulier. Un moyen pratique
de retrouver des équipements équivalents à un autre.


## Statistiques – Équipements

Pour ce qui est des statistiques des équipements, on récupère l’historique de tout ce qu’il s’est passé
avec cet équipement, tous les chantiers et personnes qui ont eu ce même équipement.


# Installation du logiciel - Aquatair

## Installation du projet pour le développement
### Cloner le projet avec SSH

``git clone "git@gitlab.com:cinquin_andy_freelance/aquatair.git"``

### Cloner le projet avec HTTPS
``git clone "https://gitlab.com/cinquin_andy_freelance/aquatair.git"``

### Installer les dépendances nodeJS
``npm install`` ou
``yarn install``

### Lancer le projet en local sur le port 3000 (Défaut)
``npm start`` ou ``yarn start``