Logiciel Aquatair gestion

# Présentation technique du logiciel

Le logiciel a été développer en React JS avec différentes librairies afin d’ avoir un logiciel facile à
utiliser et le plus performant possible.

Voici la liste des technologies utilisées dans l’application react :

- Font awesome
- Bootstrap
- Jquery
- Formik - https://github.com/formium/formik
- Hookform - https://github.com/react-hook-form/react-hook-form

Formik & Hookform -> nous permet de récupérer des informations avec les formulaires, de les traiter
plus efficacement (nous utilisons ces librairies pour les parties d’ extensions des données pour chaque
élément affiché à l’écran, avec le CRUD) , et pour l’ajout de données.

- Libphonenumber-js - https://github.com/catamphetamine/libphonenumber-js

Nous permet d’avoir une saisie plus réaliste par rapport aux numéros de téléphone, plus agréable à
utiliser du moins.

- Moment - https://github.com/moment/moment

Nous permet de gérer les dates efficacement

- Multiselect react drop down - https://github.com/srigar/multiselect-react-dropdown

Nous permet de géré la partie assignations, avec les sélections multiples pour les assignations de
plusieurs équipements à la fois

- React-bootstrap - https://github.com/react-bootstrap/react-bootstrap

Nous permet l’affichage des données de toutes les catégories, la recherche, le tri, et la gestion
entière de toutes les données.

- Yup - https://github.com/jquense/yup

Nous permet d’avoir des validations de formulaires. Champ obligatoire, message d’erreur, et c.

Pour ce qui est de l’API, c’est SAFRS qui a été utilisé (génération d’une API en python flask, avec
énormément d’éléments automatisés !)

Plus d’information sur le git dédié à l’ API.