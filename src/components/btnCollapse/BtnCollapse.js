import React from "react";
import "./BtnCollapse.css"
import ArrowBlue from "../../ressources/icons/arrowDown.svg";

class BtnCollapse extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            transform: false
        }
        this.changeTransform = this.changeTransform.bind(this);
    }

    // Cette fonction permet de changer le 'state' du bouton, et donc de changer la classe CSS de ce dernier
    // C'est comme ceci qu'on change l'orientation de la flèche lorsqu'on clique dessus.
    changeTransform() {
        this.setState(state => ({transform: !this.state.transform}));
    }

    render() {
        let orientation_btn = this.state.transform ? "btnUp" : "btnDown";
        return (
            <div onClick={this.changeTransform}>
                <img src={ArrowBlue} className={orientation_btn + " btnCollapse"} alt={"arrow"}/>
            </div>
        );
    }
}

export default BtnCollapse;