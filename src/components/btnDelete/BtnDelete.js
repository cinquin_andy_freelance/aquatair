import React from "react";
import "./BtnDelete.css"
import LogoBtnDelete from "../../ressources/icons/rubbish-bin-delete-buttonRed.svg";
import APICall from "../../function/APICall";

class BtnDelete extends React.Component {

    // La fonction qui permet de supprimer un élément ciblé par son ID
    suppressionApi = () => {
        console.log(`/${this.props.type}/${this.props.idCible}`);
        APICall(`/${this.props.type}/${this.props.idCible}`, "DELETE")
            .then((res) => {
                if (res.status === 204) {
                    window.location.reload(false);
                } else {

                }
            })
            .catch((error) => {

            })
    }

    render() {
        return (
            <div>
                <img src={LogoBtnDelete} className={"actions"} alt={"supprimer"} data-toggle={"modal"}
                     data-target={"#exampleModal" + this.props.idCible}/>
                <div className="modal fade" id={"exampleModal" + this.props.idCible} tabIndex="-1" role="dialog"
                     aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <div>Voulez vous vraiment supprimer cet élément ?</div>
                                <button type="button" className="close" data-dismiss={"modal"} aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className={"container"}>
                                <div className={"row justify-content-around"}>
                                    <button type="button" className="btn btn-secondary col-4" data-dismiss={"modal"}>
                                        Annuler
                                    </button>
                                    <button type="button" className="btn btn-danger col-4" data-dismiss={"modal"}
                                            onClick={this.suppressionApi}>
                                        Supprimer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BtnDelete;