import React from "react";
import "./BtnGrpDelete.css"
import LogoDel from "../../ressources/icons/btnSuppr.svg"
import APICall from "../../function/APICall";

function BtnGrpDelete(props) {
    // Fonction de suppression de groupe, avec un reload de page
    // Lorsqu'on change le contenu du CRUD directement, ou par changement d'objet, un problème très certainement
    // lié à React survient, à savoir que même avec avec des 'deep' copy des objets, le problème persiste.
    const suppressionApi = async () => {
        var call = props.type;

        for (const elem in props.arrayToDelete) {
            console.log(`/${call}/${props.arrayToDelete[elem]}`);
            await APICall(`/${call}/${props.arrayToDelete[elem]}`, "DELETE")
                .then((res) => {
                    if (res.status === 204) {

                    } else {

                    }
                })
                .catch((error) => {

                })
            window.location.reload(false);
        }
    }

    return (
        <div>
            <button className={"buttonGrpDelete button-del"}>
                <img src={LogoDel} className={"LogoDel"} alt={"supprimer"} data-toggle={"modal"}
                     data-target={"#exampleModalGeneral"}/>
                Supprimer
            </button>
            <div className="modal fade" id={"exampleModalGeneral"} tabIndex="-1" role="dialog"
                 aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <div>Voulez vous vraiment supprimer tous ces éléments ?</div>
                            <button type="button" className="close" data-dismiss={"modal"} aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className={"container"}>
                            <div className={"row justify-content-around"}>
                                <button type="button" className="btn btn-secondary col-4" data-dismiss={"modal"}>
                                    Annuler
                                </button>
                                <button type="button" className="btn btn-danger col-4" data-dismiss={"modal"}
                                        onClick={suppressionApi}>
                                    Supprimer
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BtnGrpDelete;