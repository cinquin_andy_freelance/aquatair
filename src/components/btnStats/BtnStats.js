import React from "react";
import "./BtnStats.css"
import LogoBtnStats from "../../ressources/icons/statsBlue.svg";
import {Link} from "react-router-dom";

function BtnStats(props) {
    return (
        // envoie sur la page de statistique
        <div>
            <Link to={`${props.categorieCible}/${props.idCible}`}>
                <img src={LogoBtnStats} className="actions" alt="stats"/>
            </Link>
        </div>
    );
}

export default BtnStats;