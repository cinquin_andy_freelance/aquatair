import React from 'react';
import APICall from "../../function/APICall";
import dlSvg from "../../ressources/icons/btnSaveFat.svg";
import './Downloader.css';


class Downloader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allData: [],
            gearData: [],
            categoryData: [],
            assignData: [],
            authorData: [],
            siteData: [],
        };
    }

    // Fonction pour récupérer les tables nécessaires de la base de données avant la transformation en csv.
    loadData = async (path, field) => {
        await APICall(path, 'GET')
            .then(res => res.json())
            .then((res) => {
                switch (field) {
                    default:
                        break;
                    case 'all':
                        this.setState({allData: res.data});
                        break;
                    case 'gear':
                        this.setState({gearData: res.data});
                        break;
                    case 'category':
                        this.setState({categoryData: res.data});
                        break;
                    case 'affectation':
                        this.setState({assignData: res.data});
                        break;
                    case 'acteur':
                        this.setState({authorData: res.data});
                        break;
                    case 'site':
                        this.setState({siteData: res.data});
                        break;

                }
            }).catch(() => {
                console.error();
            })
    }

    // On formate les données au format csv, et on place les colonnes des tables.
    convertArrayOfObjectsToCSV(args) {
        let result, keys, attributesKeys, columnDelimiter, lineDelimiter, data;

        data = args.data || null;
        if (data == null || !data.length) {
            return null;
        }
        columnDelimiter = args.columnDelimiter || ';';
        lineDelimiter = args.lineDelimiter || '\n';
        keys = Object.keys(data[0]);
        attributesKeys = Object.keys(data[0]['attributes'])
        result = '';
        keys.forEach(function (key) {
            switch (key) {
                default:
                    break;
                case 'attributes':
                    attributesKeys.forEach(function (attribut) {
                        result += attribut;
                        result += columnDelimiter;
                    })
                    break;
                case 'id':
                    result += key;
                    result += columnDelimiter;
                    break;
                case 'links':
                    break;
                case 'relationships':
                    break;
                case 'type':
                    result += key;
                    result += columnDelimiter;
                    break;
            }
        })
        result += lineDelimiter;

        data.forEach(function (item) {
            keys.forEach(function (key) {
                if (key === 'attributes') {
                    attributesKeys.forEach(function (attrKey) {
                        result += item[key][attrKey];
                        result += columnDelimiter;
                    })
                } else if (key !== 'links' && key !== 'relationships' && key !== 'attributes') {
                    result += item[key];
                    result += columnDelimiter;
                }
            });
            result += lineDelimiter;
        });
        return result;
    }

    // Fonction de download qui appel la fonction pour charger les données +
    // appel la fonction de conversion avec les données récupérés
    async downloadCSV(args, type) {
        let data, filename, link, csv;
        await this.loadData('/' + type, type);
        switch (type) {
            default:
                break;
            case 'all':
                csv = this.convertArrayOfObjectsToCSV({
                    data: this.state.allData
                });
                break;
            case 'gear':
                csv = this.convertArrayOfObjectsToCSV({
                    data: this.state.gearData
                });
                break;
            case 'category':
                csv = this.convertArrayOfObjectsToCSV({
                    data: this.state.categoryData
                });
                break;
            case 'affectation':
                csv = this.convertArrayOfObjectsToCSV({
                    data: this.state.assignData
                });
                break;
            case 'acteur':
                csv = this.convertArrayOfObjectsToCSV({
                    data: this.state.authorData
                });
                break;
            case 'site':
                csv = this.convertArrayOfObjectsToCSV({
                    data: this.state.siteData
                });
                break;
        }
        if (csv == null) return;
        filename = args.filename || type + '.csv';
        if (!csv.match(/^data:text\/csv/i)) {
            csv = 'data:text/csv;charset=utf-8,' + csv;
        }
        data = encodeURI(csv);
        link = document.createElement('a');
        link.setAttribute('href', data);
        link.setAttribute('download', filename);
        link.click();
    }

    render() {
        return (
            <button className="download-icon">
                <li>
                    <img src={dlSvg} alt="Download icons"/>
                    <div className="download-menu">
                        <ul>
                            <li onClick={() => this.downloadCSV('save', 'gear')}>Télécharger équipements CSV</li>
                            <li onClick={() => this.downloadCSV('save', 'category')}>Télécharger catégories CSV</li>
                            <li onClick={() => this.downloadCSV('save', 'site')}>Télécharger chantiers CSV</li>
                            <li onClick={() => this.downloadCSV('save', 'acteur')}>Télécharger personnes CSV</li>
                            <li onClick={() => this.downloadCSV('save', 'affectation')}>Télécharger affectations CSV
                            </li>
                            <li onClick={() => this.downloadCSV('save', 'gear')
                                .then(() => this.downloadCSV('save', 'category')
                                    .then(() => this.downloadCSV('save', 'site')
                                        .then(() => this.downloadCSV('save', 'acteur')
                                            .then(() => this.downloadCSV('save', 'affectation')
                                            )
                                        )
                                    )
                                )
                            }>
                                Tout télécharger
                            </li>
                        </ul>
                    </div>
                </li>
            </button>
        );
    }
}

export default Downloader;