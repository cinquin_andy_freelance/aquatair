import React from "react";
import "./Expanded.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import ExpandedCategories from "./expandedCategories/ExpandedCategories";
import ExpandedPeople from "./expandedPeople/ExpandedPeople";
import ExpandedSites from "./expandedSites/ExpandedSites";
import ExpandedGears from "./expandedGears/ExpandedGears";
import ExpandedAssigns from "./expandedAssigns/ExpandedAssigns";


// on passera le type de champs visée par Expanded, et le type de champ traité par le crud à ce moment la
function Expanded(props) {
    // On choisis ici quel expand affiché en fonction du type qui lui est passer
    function chooseTypeDisplayed(typeField) {
        switch (typeField) {
            case 'category':
                return (
                    <ExpandedCategories
                        row={props.values}
                        id={props.values.id}
                        shortId={props.values.shortId}
                        name={props.values.name}
                        description={props.values.description}
                        handle={props.handle}
                    />
                )
            case 'acteur':
                return (
                    <ExpandedPeople
                        row={props.values}
                        id={props.values.id}
                        lastname={props.values.lastname}
                        firstname={props.values.firstname}
                        phoneNumber={props.values.phoneNumber}
                        handle={props.handle}
                    />
                )
            case 'site':
                return (
                    <ExpandedSites
                        row={props.values}
                        id={props.values.id}
                        name={props.values.name}
                        city={props.values.city}
                        address={props.values.address}
                        description={props.values.description}
                        handle={props.handle}
                    />
                )
            case 'gear':
                return (
                    <ExpandedGears
                        row={props.values}
                        id={props.values.id}
                        shortId={props.values.shortId}
                        name={props.values.name}
                        type={props.values.gearType}
                        mark={props.values.mark}
                        usury={props.values.usury}
                        purchaseDate={props.values.purchaseDate}
                        endLifeDate={props.values.endLifeDate}
                        description={props.values.description}
                        categoryId={props.values.categoryId}
                        handle={props.handle}
                    />
                )
            case 'affectation':
                return (
                    <ExpandedAssigns
                        row={props.values}
                        gearId={props.values.gearId}
                        acteurId={props.values.acteurId}
                        siteId={props.values.siteId}
                        startDateAssignation={props.values.startDateAssignation}
                        endDateAssignation={props.values.endDateAssignation}
                        handle={props.handle}
                    />
                )
            default:
                return null;
        }
    }

    return (
        <div className={"container-fluid"}>
            {chooseTypeDisplayed(props.type)}
        </div>
    );

}

export default Expanded;