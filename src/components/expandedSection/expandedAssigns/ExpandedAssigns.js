import React, {useEffect, useState} from 'react';
import '../Expanded.css';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import btnValidSvg from '../../../ressources/icons/btnValid.svg';
import APICall from "../../../function/APICall";
import moment from "moment";

// On creer le schéma des formulaires pour l'expand des assignations
const schema = yup.object().shape({
    id: yup.string(),
    acteurId: yup.string(),
    siteId: yup.string(),
    startDateAssignation: yup.date().typeError('Entrez une date valide : jj/mm/aaaa').required('Veuillez entrer la date d\'obtention'),
    endDateAssignation: yup.date().nullable().transform((curr, orig) => orig === '' ? null : curr).typeError('Entrez une date valide : jj/mm/aaaa'),
}).test('xor', 'Vous devez soit sélectionner une personne soit un chantier', val => {
    // un Acteur , ou un Chantier, mais pas les deux !
    return ((val.acteurId !== null && val.acteurId !== 'none') ^ (val.siteId !== null && val.siteId !== 'none')) ? true : false;
})

function ExpandedAssigns(props) {
    // Les infos de l'équipement seront affichés grace à cette fonction
    // Afin d'avoir les infos qui correspondes à 'l'étiquette' de l'équipement.
    function displayInfoGear(idGear) {
        let display = "";
        for (const elem of gear) {
            if (elem.id === idGear) {
                display = elem.displayableValue;
            }
        }
        return display;
    }

    var dateStateStartAssignation = moment(props.startDateAssignation);
    var dateStateEndAssignation = moment(props.endDateAssignation);

    const [idState] = useState(props.row.id);
    const [gearIdState] = useState(props.gearId);
    const [acteurIdState] = useState(props.acteurId);
    const [siteIdState] = useState(props.siteId);
    const [startDateAssignationState] = useState(dateStateStartAssignation.format("YYYY-MM-DD"));
    const [endDateAssignationState] = useState(dateStateEndAssignation.format("YYYY-MM-DD"));

    const [gear, setGear] = useState([]);
    const [acteur, setActeur] = useState([]);
    const [site, setSite] = useState([]);
    const [category, setCategory] = useState([]);

    const [message, setMessage] = useState('');

    const changeState = (message) => {
        setMessage(message);
    }

    // Fonction utilisé pour ne pas boucler indéfiniment sur les fonctions de load(Gears, Acteurs, Sites).
    useEffect(() => {
        loadCategories();
        loadActeurs();
        loadSites();
        // eslint-disable-next-line
    }, [])
    useEffect(() => {
        loadGears();
        // eslint-disable-next-line
    }, [category])

    // On indique le comportement du formulaire
    const {register, handleSubmit, formState, errors, setValue} =
        useForm({
            mode: 'onTouched',
            resolver: yupResolver(schema),
            reset: false,
            defaultValues: {
                gearId: gearIdState,
                acteurId: acteurIdState,
                siteId: siteIdState,
                startDateAssignation: startDateAssignationState,
                endDateAssignation: endDateAssignationState
            }
        });

    const {isSubmitting} = formState;

    const loadGears = async () => {
        await APICall('/gear', 'GET')
            .then(res => res.json())
            .then((res) => {
                const arrayGear = [];
                res.data.map((data) => {
                    let catShortId = '';
                    for (const cat of category) {
                        if (cat.id === data.attributes.categoryId) {
                            catShortId = cat.shortId;
                        }
                    }
                    arrayGear.push({
                        id: data.id,
                        shortId: data.attributes.shortId,
                        name: data.attributes.name,
                        mark: data.attributes.mark,
                        displayableValue: `Référence (simple) : ${catShortId}_${data.attributes.shortId} || Nom : ${data.attributes.name} || Identifiant : ${(data.id).substring(0, 8)}`
                    })
                });
                // res.data.map(data => arrayGear.push({
                //     id: data.id,
                //     shortId: data.attributes.shortId,
                //     name: data.attributes.name,
                //     mark: data.attributes.mark,
                //     displayableValue: `Référence (simple) : ${data.attributes.shortId.toUpperCase()} || Nom : ${data.attributes.name} || Identifiant : ${(data.id).substring(0, 8)}`
                // }));
                setGear(arrayGear);
            }).catch((error) => {

            })
    }

    const loadActeurs = async () => {
        await APICall('/acteur', 'GET')
            .then(res => res.json())
            .then((res) => {
                const arrayActeur = [];
                res.data.map(data => arrayActeur.push({
                    id: data.id,
                    firstname: data.attributes.firstname,
                    lastname: data.attributes.lastname,
                    phoneNumber: data.attributes.phoneNumber
                }));
                setActeur(arrayActeur);
            }).catch((error) => {

            })
    }

    const loadSites = async () => {
        await APICall('/site', 'GET')
            .then(res => res.json())
            .then((res) => {
                const arraySite = [];
                res.data.map(data => arraySite.push({
                    id: data.id,
                    name: data.attributes.name,
                    city: data.attributes.city,
                    address: data.attributes.address,
                    description: data.attributes.description
                }));
                setSite(arraySite);
            }).catch((error) => {

            })
    }

    const loadCategories = async () => {
        await APICall('/category', 'GET')
            .then(res => res.json())
            .then((res) => {
                const arrayCat = [];
                res.data.map(data => arrayCat.push({
                    id: data.id,
                    shortId: data.attributes.shortId,
                    name: data.attributes.name,
                    description: data.attributes.description,
                }));
                setCategory(arrayCat);
            }).catch(() => {
                console.error();
            })
    }

    const onSubmit = async data => {
        const dataFormat = {
            "data": {
                "attributes": {
                    "startDateAssignation": moment(data.startDateAssignation).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                    "endDateAssignation": moment(data.endDateAssignation).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                    "gearId": data.gearId,
                    "acteurId": data.acteurId,
                    "siteId": data.siteId
                },
                "type": "Affectation"
            }
        };

        // Si la date de début est invalide on la met sur la date du jour (ça evite un potentiel manque de saisie, ou
        // une saisie impossible, ce qui ne, devrait, théoriquement, jamais arrivé.
        if (dataFormat.data.attributes.startDateAssignation === "Invalid date") {
            dataFormat.data.attributes.startDateAssignation = moment.now();
        }
        if (dataFormat.data.attributes.endDateAssignation === "Invalid date") {
            dataFormat.data.attributes.endDateAssignation = null;
        }
        if (dataFormat.data.attributes.siteId === "none") {
            dataFormat.data.attributes.siteId = null;
        }
        if (dataFormat.data.attributes.acteurId === "none") {
            dataFormat.data.attributes.acteurId = null;
        }

        // On lance la requete vers l'api pour mettre à jour les valeurs dans la base de donnée
        await patchAffectation(dataFormat, props.row.id)

        // On remplace les données du tableau actuel, pour mettre à jour en local,
        // sans re- call l'api
        props.handle({
            ...props.row,
            acteurId: data.acteurId,
            siteId: data.siteId,
            startDateAssignation: data.startDateAssignation,
            endDateAssignation: data.endDateAssignation,
        })
    }

    const patchAffectation = async (object, idCible) => {
        // On formate l'objet à envoyer vers l'api
        var object = {
            data: {
                attributes: object.data.attributes,
                type: "Affectation",
                id: idCible
            }
        };
        await APICall("/affectation/" + idCible, "PATCH", object)
            .then((res) => {
                if (res.status === 201 || res.status === 200) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000))

                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000))

                }
            })
            .catch((error) => {

            });
    }

    return (
        <div className="container py-5">
            <input type="hidden" value={props.row.id} name={"id"} id={"id"}/>
            <div>
                <label htmlFor="gearDisplay" className={"font-weight-bold"}>Equipement : </label>
                <input value={displayInfoGear(props.gearId)} type="string" readOnly={true} className="form-control"
                       id="gearDisplay"
                       name="gearDisplay" ref={register}/>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                {message}
                <label className={"m-2 font-weight-bold"}>
                    Vous ne pouvez séléctionnez qu'une personne ou un chantier, soit l'un, soit l'autre. pas les deux.
                </label>
                <div className="row">
                    <div className="col-sm form-group">
                        <label htmlFor="acteurId">Personne</label>
                        <select
                            className="form-control"
                            name="acteurId"
                            id="acteurId"
                            onChange={() => {
                                setValue("siteId", "none")
                            }}
                            ref={register}>
                            <option value="none">Sélectionnez une personne</option>
                            {
                                acteur.map(
                                    e => (e.id === props.acteurId) ? <option selected={true} key={e.id}
                                                                             value={e.id}>{e.lastname + ' ' + e.firstname}</option> :
                                        <option key={e.id} value={e.id}>{e.lastname + ' ' + e.firstname}</option>
                                )
                            }
                        </select>
                        {errors.acteurId && <span>{errors.acteurId.message}</span>}
                    </div>
                    <div className="col-sm form-group">
                        <label htmlFor="siteId">Chantier</label>
                        <select
                            className="form-control"
                            name="siteId"
                            id="siteId"
                            onChange={() => {
                                setValue("acteurId", "none")
                            }}
                            ref={register}>
                            <option value="none">Sélectionnez un chantier</option>
                            {
                                site.map(
                                    e =>
                                        (e.id === props.siteId) ? <option selected={true} key={e.id}
                                                                          value={e.id}>{e.name + ' ' + e.city}</option> :
                                            <option key={e.id} value={e.id}>{e.name + ' ' + e.city}</option>
                                )
                            }
                        </select>
                        {errors.siteId && <span>{errors.siteId.message}</span>}
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm form-group">
                        <label htmlFor="startDateAssignation">Date début affectation</label>
                        <input type="date" className="form-control" id="startDateAssignation"
                               placeholder="Date : JJ/MM/YYYY" name="startDateAssignation"
                               ref={register}/>
                        {errors.startDateAssignation && <span>{errors.startDateAssignation.message}</span>}
                    </div>
                    <div className="col-sm form-group">
                        <label htmlFor="endDateAssignation">Date fin affectation</label>
                        <input type="date" className="form-control" id="endDateAssignation"
                               placeholder="Date : JJ/MM/YYYY" name="endDateAssignation"
                               ref={register}/>
                        {errors.endDateAssignation && <span>{errors.endDateAssignation.message}</span>}
                    </div>
                </div>
                <div className="button-group">
                    <button disabled={isSubmitting} type="submit" className="btn btn-add">
                        <img src={btnValidSvg} alt="icones bouton ajout"/>
                        Mettre à jour
                    </button>
                </div>
            </form>
        </div>
    )
}

export default ExpandedAssigns;