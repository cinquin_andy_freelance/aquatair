import React, {useState} from 'react';
import '../Expanded.css';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import btnValidSvg from '../../../ressources/icons/btnValid.svg';
import APICall from "../../../function/APICall";
import StringFormat from "../../../function/StringFormat";

// On creer le schéma des formulaires pour l'expand des catégories
const schema = yup.object().shape({
    shortId: yup.string().required('Veuillez entrer la référence'),
    name: yup.string().required('Veuillez entrer un nom'),
    description: yup.string(),
})

function ExpandedCategories(props) {
    const [shortIdState] = useState((props.shortId).toUpperCase());
    const [nameState] = useState(StringFormat(props.name));
    const [descriptionState] = useState(props.description);

    const [message, setMessage] = useState('');

    const changeState = (message) => {
        setMessage(message);
    }

    // On indique le comportement du formulaire
    const {register, handleSubmit, formState, errors} =
        useForm({
            mode: 'onTouched',
            resolver: yupResolver(schema),
            defaultValues: {
                shortId: shortIdState,
                name: nameState,
                description: descriptionState
            }
        });

    const {isSubmitting} = formState;
    const onSubmit = data => {
        const dataFormat = {
            "data": {
                "attributes": {
                    "shortId": (data.shortId).toUpperCase(),
                    "name": StringFormat(data.name),
                    "description": data.description
                },
                "type": "Category"
            }
        };

        // On lance la requete vers l'api pour mettre à jour les valeurs dans la base de donnée
        patchNewWorksite(dataFormat, props.id);

        // On remplace les données du tableau actuel, pour mettre à jour en local,
        // sans re- call l'api
        props.handle({
            ...props.row,
            shortId: data.shortId,
            name: data.name,
            description: data.description
        })
    }

    const patchNewWorksite = (data, idCible) => {
        // On formate l'objet à envoyer vers l'api
        var object = {
            data: {
                attributes: data.data.attributes,
                type: "Category",
                id: idCible
            }
        };
        APICall("/category/" + idCible, "PATCH", object)
            .then((res) => {
                if (res.status === 201 || res.status === 200) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000))

                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000))

                }
            })
            .catch((error) => {

            })
    }

    return (
        <form className="container py-5" onSubmit={handleSubmit(onSubmit)}>
            {message}
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="shortId">Référence</label>
                    <input type="text" className="form-control" id="shortId"
                           placeholder="Référence (Simple)" name="shortId"
                           ref={register}/>
                    {errors.idSimple && <span>{errors.idSimple.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="name">Nom</label>
                    <input type="text" className="form-control" id="name"
                           placeholder="Nom" name="name"
                           ref={register}/>
                    {errors.name && <span>{errors.name.message}</span>}
                </div>
            </div>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="description">Description</label>
                    <textarea className="form-control" id="description"
                              placeholder="Description" name="description"
                              ref={register}/>
                    {errors.description && <span>{errors.description.message}</span>}
                </div>
            </div>
            <div className="button-group">
                <button disabled={isSubmitting} type="submit" className="btn btn-add">
                    <img src={btnValidSvg} alt="icones bouton ajout"/>
                    Mettre à jour
                </button>
            </div>
        </form>
    )
}

export default ExpandedCategories;