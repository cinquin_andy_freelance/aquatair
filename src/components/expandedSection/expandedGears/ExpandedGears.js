import React, {useEffect, useState} from 'react';
import '../Expanded.css';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import btnValidSvg from '../../../ressources/icons/btnValid.svg';
import APICall from "../../../function/APICall";
import moment from "moment";
import StringFormat from "../../../function/StringFormat";


// On creer le schéma des formulaires pour l'expand des équipements
const schema = yup.object().shape({
    shortId: yup.string().required('Veuillez entrer la référence'),
    name: yup.string().required('Veuillez entrer un nom'),
    type: yup.string(),
    mark: yup.string(),
    usury: yup.string(),
    purchaseDate: yup.date().required('Veuillez entrer la date d\'obtention'),
    endLifeDate: yup.date().nullable().transform((curr, orig) => orig === '' ? null : curr).typeError('Entrez une date valide : jj/mm/aaaa'),
    description: yup.string(),
    categoryId: yup.string().ensure().required('Veuillez sélectionner une catégorie'),
});

function ExpandedGears(props) {
    const dateStatePurchase = moment(props.purchaseDate);
    const dateStateEndLife = moment(props.endLifeDate);

    const [shortIdState] = useState((props.shortId).toUpperCase());
    const [nameState] = useState(StringFormat(props.name));
    const [typeState] = useState(StringFormat(props.type));
    const [markState] = useState(StringFormat(props.mark));
    const [usuryState] = useState(StringFormat(props.usury));
    const [purchaseDateState] = useState(dateStatePurchase.format("YYYY-MM-DD"));
    const [endLifeDateState] = useState(dateStateEndLife.format("YYYY-MM-DD"));
    const [descriptionState] = useState(props.description);
    const [categoryId, setCategory] = useState(props.categoryId);
    const [message, setMessage] = useState('');
    const [array, setArray] = useState([]);

    const changeState = (message) => {
        setMessage(message);
    }

    const handleChange = (event) => {
        setCategory(event.target.value)
    }

    // On charge nos catégories
    useEffect(() => {
        loadCategories();
        // eslint-disable-next-line
    }, [])


    // On indique le comportement du formulaire
    const {register, handleSubmit, formState, errors} = useForm(
        {
            mode: 'onTouched',
            resolver: yupResolver(schema),
            defaultValues: {
                shortId: shortIdState,
                name: nameState,
                type: typeState,
                mark: markState,
                usury: usuryState,
                purchaseDate: purchaseDateState,
                endLifeDate: endLifeDateState,
                description: descriptionState,
                categoryId: categoryId
            }
        });

    const {isSubmitting} = formState;

    const loadCategories = async () => {
        await APICall('/category', 'GET')
            .then(res => res.json())
            .then((res) => {
                showCategories(res.data);
            }).catch(() => {
                console.error();
            })
    }

    function showCategories(data) {
        let toShow = [];
        for (const el of data) {
            toShow.push(<option key={el.id} value={el.id}>Référence : {el.attributes.shortId}; Nom
                : {el.attributes.name}</option>)
        }
        setArray(toShow)
    }

    const onSubmit = async data => {
        const dataFormat = {
            "data": {
                "attributes": {
                    "shortId": data.shortId.toUpperCase(),
                    "name": StringFormat(data.name),
                    "gearType": StringFormat(data.type),
                    "mark": StringFormat(data.mark),
                    "usury": StringFormat(data.usury),
                    "purchaseDate": moment(data.purchaseDate).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                    "endLifeDate": moment(data.endLifeDate).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                    "description": data.description,
                    "categoryId": data.categoryId
                },
                "type": "Gear"
            }
        };
        if (dataFormat.data.attributes.purchaseDate === "Invalid date") {
            dataFormat.data.attributes.purchaseDate = moment.now();
        }
        if (dataFormat.data.attributes.endLifeDate === "Invalid date") {
            dataFormat.data.attributes.endLifeDate = null;
        }

        // On lance la requete vers l'api pour mettre à jour les valeurs dans la base de donnée
        await patchNewGear(dataFormat, props.id);

        // On remplace les données du tableau actuel, pour mettre à jour en local,
        // sans re- call l'api
        props.handle({
            ...props.row,
            shortId: data.shortId,
            name: StringFormat(data.name),
            gearType: StringFormat(data.gearType),
            mark: StringFormat(data.mark),
            usury: StringFormat(data.usury),
            purchaseDate: data.purchaseDate,
            endLifeDate: data.endLifeDate,
            description: data.description,
            categoryId: data.categoryId,
        })
    }

    const patchNewGear = async (data, idCible) => {
        // On formate l'objet à envoyer vers l'api
        var object = {
            data: {
                attributes: data.data.attributes,
                type: "Gear",
                id: idCible
            }
        };
        await APICall("/gear/" + idCible, "PATCH", object)
            .then((res) => {
                if (res.status === 201 || res.status === 200) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000))
                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000))
                }
            })
            .catch((error) => {

            });
    }

    return (
        <form className="container py-5" onSubmit={handleSubmit(onSubmit)}>
            {message}
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="shortId">Référence</label>
                    <input type="text" className="form-control" id="shortId"
                           placeholder="Référence (Simple)" name="shortId"
                           ref={register}/>
                    {errors.idSimple && <span>{errors.idSimple.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="name">Nom</label>
                    <input type="text" className="form-control" id="name"
                           placeholder="Nom" name="name"
                           ref={register}/>
                    {errors.name && <span>{errors.name.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="type">Type</label>
                    <input type="text" className="form-control" id="type"
                           placeholder="Type" name="type"
                           ref={register}/>
                    {errors.type && <span>{errors.type.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="mark">Marque</label>
                    <input type="text" className="form-control" id="mark"
                           placeholder="Marque" name="mark"
                           ref={register}/>
                    {errors.mark && <span>{errors.mark.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="usury">Usure</label>
                    <input type="text" className="form-control" id="usury"
                           placeholder="État d'usure" name="usury"
                           ref={register}/>
                    {errors.usury && <span>{errors.usury.message}</span>}
                </div>
            </div>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="purchaseDate">Date d'obtention</label>
                    <input type="date" className="form-control" id="purchaseDate"
                           placeholder="Date : JJ/MM/YYYY" name="purchaseDate"
                           ref={register({valueAsDate: true})}/>
                    {errors.purchaseDate && <span>{errors.purchaseDate.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="endLifeDate">Date de rendu</label>
                    <input type="date" className="form-control" id="endLifeDate"
                           placeholder="Date : JJ/MM/YYYY" name="endLifeDate"
                           ref={register}/>
                    {errors.endLifeDate && <span>{errors.endLifeDate.message}</span>}
                </div>
            </div>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="description">Description</label>
                    <textarea className="form-control" id="description"
                              placeholder="Description" name="description"
                              ref={register}/>
                    {errors.description && <span>{errors.description.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="categoryId">Catégorie liée</label>
                    <select value={categoryId} onChange={handleChange} className="form-control" name="categoryId"
                            id="categoryId" ref={register}>
                        {/*<option>Sélectionnez une catégorie</option>*/}
                        {array}
                    </select>
                    {errors.categoryId && <span>{errors.categoryId.message}</span>}
                </div>
            </div>
            <div className="button-group">
                <button disabled={isSubmitting} type="submit" className="btn btn-add">
                    <img src={btnValidSvg} alt="icones bouton ajout"/>
                    Mettre à jour
                </button>
            </div>
        </form>
    )
}

export default ExpandedGears;