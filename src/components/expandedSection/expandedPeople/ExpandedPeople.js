import React, {useState} from 'react';
import '../Expanded.css';
import {useForm} from "react-hook-form";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import btnValidSvg from '../../../ressources/icons/btnValid.svg';
import APICall from "../../../function/APICall";
import StringFormat from "../../../function/StringFormat";
import {AsYouType} from 'libphonenumber-js'

// On creer le schéma des formulaires pour l'expand des acteurs
const schema = yup.object().shape({
    lastname: yup.string().required('Veuillez entrer un prénom'),
    firstname: yup.string().required('Veuillez entrer un prénom'),
    phoneNumber: yup.string().required('Veuillez entrer un numéro de téléphone')
})

function ExpandedPeople(props) {
    const [lastnameState] = useState((props.lastname).toUpperCase());
    const [firstnameState] = useState(StringFormat(props.firstname));
    const [phoneNumberState, setPhoneNumberState] = useState(props.phoneNumber);

    const [message, setMessage] = useState('');

    const changeState = (message) => {
        setMessage(message);
    }

    // On indique le comportement du formulaire
    const {register, handleSubmit, formState, errors} =
        useForm({
            mode: 'onTouched',
            resolver: yupResolver(schema),
            defaultValues: {
                lastname: lastnameState,
                firstname: firstnameState,
                phoneNumber: phoneNumberState
            }
        });

    const {isSubmitting} = formState;

    const formatPhoneNumber = (e) => {

        e.target.value = new AsYouType('FR').input(e.target.value);
        setPhoneNumberState(new AsYouType('FR').input(e.target.value));
    }

    const onSubmit = data => {
        const dataFormat = {
            "data": {
                "attributes": {
                    "lastname": (data.lastname).toUpperCase(),
                    "firstname": StringFormat(data.firstname),
                    "phoneNumber": data.phoneNumber
                },
                "type": "Acteur"
            }
        };

        // On lance la requete vers l'api pour mettre à jour les valeurs dans la base de donnée
        patchNewPeople(dataFormat, props.id);

        // On remplace les données du tableau actuel, pour mettre à jour en local,
        // sans re- call l'api
        props.handle({
            ...props.row,
            lastname: data.lastname,
            firstname: data.firstname,
            phoneNumber: data.phoneNumber
        })
    }

    const patchNewPeople = (data, idCible) => {
        // On formate l'objet à envoyer vers l'api
        var object = {
            data: {
                attributes: data.data.attributes,
                type: "Acteur",
                id: idCible
            }
        };
        APICall("/acteur/" + idCible, "PATCH", object)
            .then((res) => {
                if (res.status === 201 || res.status === 200) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000))

                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000))
                }
            })
            .catch((error) => {

            })
    }

    return (
        <form className="container py-5" onSubmit={handleSubmit(onSubmit)}>
            {message}
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="lastname">Nom</label>
                    <input type="text" className="form-control" id="lastname"
                           placeholder="Nom" name="lastname"
                           ref={register}/>
                    {errors.lastname && <span>{errors.lastname.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="firstname">Prénom</label>
                    <input type="text" className="form-control" id="firstname"
                           placeholder="Prénom" name="firstname"
                           ref={register}/>
                    {errors.firstname && <span>{errors.firstname.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="phoneNumber">Numéro de téléphone</label>
                    <input type="text" className="form-control" id="phoneNumber"
                           placeholder="0123456789 ou +33 0123456789" name="phoneNumber"
                           ref={register} onChange={formatPhoneNumber}/>
                    {errors.phoneNumber && <span>{errors.phoneNumber.message}</span>}
                </div>
            </div>
            <div className="button-group">
                <button disabled={isSubmitting} type="submit" className="btn btn-add">
                    <img src={btnValidSvg} alt="icones bouton ajout"/>
                    Mettre à jour
                </button>
            </div>
        </form>
    )
}

export default ExpandedPeople;