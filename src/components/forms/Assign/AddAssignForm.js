import React, {useEffect, useState} from 'react';
import '../AddForm.css';
import {useForm} from "react-hook-form";
import {useHistory} from "react-router-dom";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import {Multiselect} from 'multiselect-react-dropdown';
import btnAddSvg from '../../../ressources/icons/btnAdd.svg';
import btnCancel from '../../../ressources/icons/btnCancel.svg';
import APICall from "../../../function/APICall";
import moment from "moment";


// Schéma qui doit être respecté pour la validation du formulaire sinon il ne pourra pas être submit.
const schema = yup.object().shape({
    gearId: yup.string().required(),
    acteurId: yup.string(),
    siteId: yup.string(),
    startDateAssignation: yup.date().typeError('Entrez une date valide : jj/mm/aaaa').required('Veuillez entrer la date d\'obtention'),
    endDateAssignation: yup.date().nullable().transform((curr, orig) => orig === '' ? null : curr).typeError('Entrez une date valide : jj/mm/aaaa'),
}).test('xor', 'Vous devez soit sélectionner une personne soit un chantier', val => {
    return ((val.acteurId !== null && val.acteurId !== 'none') ^ (val.siteId !== null && val.siteId !== 'none')) ? true : false;
})

function AddAssignForm() {
    // State pour gérer le retour utilisateur si l'ajout des données c'est bien passé ou non.
    const [message, setMessage] = useState('');

    // State où on récupère tous les équipements qui existent en base de données, pour pouvoir les sélectionner sur la page d'affectation.
    const [gear, setGear] = useState([]);

    // State [] où l'on récupère les équipements sélectionnés sur le Multiselect, pour pouvoir créer les affectations par équipement ensuite.
    const [selectedValues, setSelectedValues] = useState([]);

    // State où on récupère tous les acteurs (personnes) qui existent, pour pouvoir en sélectionner un ou pas sur la page d'affectation.
    const [acteur, setActeur] = useState([]);

    // State où on récupère tous les sites (chantiers) qui existent, pour pouvoir en sélectionenr un ou pas sur la page d'affectation.
    const [site, setSite] = useState([]);

    // State où on récupère toutes les catégories qui existent.
    const [category, setCategory] = useState([]);

    const changeState = (message) => {
        setMessage(message);
    }

    const {register, handleSubmit, formState, errors, reset, setValue} =
        useForm({
            mode: 'onTouched',
            resolver: yupResolver(schema),
        });

    // useHistory() utilisé pour le bouton retour, pour retrouver l'avant dernière page visitée.
    const history = useHistory();
    const {isSubmitting} = formState;

    // Fonction asynchrone pour récupérer "GET" tous les équipements présents en base de données.
    const loadGears = async () => {
        await APICall('/gear', 'GET')
            .then(res => res.json())
            .then(async (res) => {
                const arrayGear = [];
                res.data.map((data) => {
                    let catShortId = '';
                    for (const cat of category) {
                        if (cat.id === data.attributes.categoryId) {
                            catShortId = cat.shortId;
                        }
                    }
                    arrayGear.push({
                        id: data.id,
                        shortId: data.attributes.shortId,
                        name: data.attributes.name,
                        mark: data.attributes.mark,
                        displayableValue: `Référence (simple) : ${catShortId}_${data.attributes.shortId} || Nom : ${data.attributes.name} || Identifiant : ${(data.id).substring(0, 8)}`
                    })
                });
                setGear(arrayGear);
            }).catch(() => {
                console.error();
            })
    }

    // Fonction asynchrone pour récupérer "GET" toutes les catégories présentes en base de données.
    const loadCategories = async () => {
        await APICall('/category', 'GET')
            .then(res => res.json())
            .then((res) => {
                const arrayCat = [];
                res.data.map(data => arrayCat.push({
                    id: data.id,
                    shortId: data.attributes.shortId,
                    name: data.attributes.name,
                    description: data.attributes.description,
                }));
                setCategory(arrayCat);
            }).catch(() => {
                console.error();
            })
    }

    // Fonction asynchrone pour récupérer "GET" tous les acteurs (personnes) présent en base de données.
    const loadActeurs = async () => {
        await APICall('/acteur', 'GET')
            .then(res => res.json())
            .then((res) => {
                const arrayActeur = [];
                res.data.map(data => arrayActeur.push({
                    id: data.id,
                    firstname: data.attributes.firstname,
                    lastname: data.attributes.lastname,
                    phoneNumber: data.attributes.phoneNumber
                }));
                setActeur(arrayActeur);
            }).catch(() => {
                console.error();
            })
    }

    // Fonction asynchrone pour récupérer "GET" tous les sites (chantiers) présent en base de données.
    const loadSites = async () => {
        await APICall('/site', 'GET')
            .then(res => res.json())
            .then((res) => {
                const arraySite = [];
                res.data.map(data => arraySite.push({
                    id: data.id,
                    name: data.attributes.name,
                    city: data.attributes.city,
                    address: data.attributes.address,
                    description: data.attributes.description
                }));
                setSite(arraySite);
            }).catch(() => {
                console.error();
            })
    }

    // Fonction utilisé pour ne pas boucler indéfiniment sur les fonctions de load(Gears, Acteurs, Sites).
    useEffect(() => {
        loadCategories();
        loadActeurs();
        loadSites();
        // eslint-disable-next-line
    }, [])
    useEffect(() => {
        loadGears();
        // eslint-disable-next-line
    }, [category])

    // Fonction pour formater les données avant de les envoyés "POST" en base de données.
    const onSubmit = async data => {
        const gears = data.gearId.split(',');
        gears.map(async (el) => {
            const dataFormat = {
                "data": {
                    "attributes": {
                        "startDateAssignation": moment(data.startDateAssignation).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                        "endDateAssignation": moment(data.endDateAssignation).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                        "gearId": el,
                        "acteurId": data.acteurId,
                        "siteId": data.siteId
                    },
                    "type": "Affectation"
                }
            };
            if (dataFormat.data.attributes.startDateAssignation === "Invalid date") {
                dataFormat.data.attributes.startDateAssignation = moment.now();
            }
            if (dataFormat.data.attributes.endDateAssignation === "Invalid date") {
                dataFormat.data.attributes.endDateAssignation = null;
            }
            if (dataFormat.data.attributes.siteId === "none") {
                dataFormat.data.attributes.siteId = null;
            }
            if (dataFormat.data.attributes.acteurId === "none") {
                dataFormat.data.attributes.acteurId = null;
            }
            await postNewAffectation(dataFormat)
        })
        reset();
        setSelectedValues([]);
    }

    // Fonction pour envoyés les données formatées en base de données.
    const postNewAffectation = async (data) => {
        await APICall("/affectation", "POST", data)
            .then((res) => {
                if (res.status === 200 || res.status === 201) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000));

                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000));

                }
            })
            .catch(() => {
                console.error();
            });
    }

    return (
        <form className="container py-5" onSubmit={handleSubmit(onSubmit)}>
            {message}

            <div className="row">
                <label className={"m-2 font-weight-bold"}>
                    Vous pouvez séléctionnez plusieurs équipements à la fois
                </label>
                <Multiselect
                    options={gear}
                    closeOnSelect={false}
                    selectedValues={selectedValues}
                    onSelect={(selection) => {
                        setSelectedValues(selection)
                    }}
                    onRemove={(deleted) => {
                        setSelectedValues(deleted)
                    }}
                    showCheckbox={true}
                    placeholder="Équipements"
                    displayValue={"displayableValue"}
                    avoidHighlightFirstOption={true}
                />
                <input readOnly={true} className="form-control" type="hidden" name="gearId" id="gearId"
                       value={selectedValues.map(e => e.id)} ref={register}/>
            </div>

            <label className={"m-2 font-weight-bold"}>
                Vous ne pouvez séléctionnez qu'une personne ou un chantier, soit l'un, soit l'autre. pas les deux.
            </label>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="acteurId">Personne</label>
                    <select
                        className="form-control"
                        name="acteurId"
                        id="acteurId"
                        onChange={() => {
                            setValue("siteId", "none")
                        }}
                        ref={register}>
                        <option value="none">Sélectionnez une personne</option>
                        {acteur.map(e => <option key={e.id} value={e.id}>{e.lastname + ' ' + e.firstname}</option>)}
                    </select>
                    {errors.acteurId && <span>{errors.acteurId.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="siteId">Chantier</label>
                    <select
                        className="form-control"
                        name="siteId"
                        id="siteId"
                        onChange={() => {
                            setValue("acteurId", "none")
                        }}
                        ref={register}>
                        <option value="none">Sélectionnez un chantier</option>
                        {site.map(e => <option key={e.id} value={e.id}>{e.name + ' ' + e.city}</option>)}
                    </select>
                    {errors.siteId && <span>{errors.siteId.message}</span>}
                </div>
            </div>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="startDateAssignation">Date début affectation</label>
                    <input type="date" className="form-control" id="startDateAssignation"
                           placeholder="Date : JJ/MM/YYYY" name="startDateAssignation"
                           ref={register}/>
                    {errors.startDateAssignation && <span>{errors.startDateAssignation.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="endDateAssignation">Date fin affectation</label>
                    <input type="date" className="form-control" id="endDateAssignation"
                           placeholder="Date : JJ/MM/YYYY" name="endDateAssignation"
                           ref={register}/>
                    {errors.endDateAssignation && <span>{errors.endDateAssignation.message}</span>}
                </div>
            </div>
            <div className="button-group">
                <button disabled={isSubmitting} type="button" onClick={() => {
                    history.goBack()
                }} className="btn btn-cancel">
                    <img src={btnCancel} alt="icones bouton retour"/>
                    Annuler
                </button>
                <button disabled={isSubmitting} type="submit" className="btn btn-add">
                    <img src={btnAddSvg} alt="icones bouton ajout"/>
                    Ajouter
                </button>
            </div>
        </form>
    )
}

export default AddAssignForm;