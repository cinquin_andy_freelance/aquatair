import React, {useEffect, useState} from "react";
import BtnStats from "../../btnStats/BtnStats";
import BtnDelete from "../../btnDelete/BtnDelete";
import BtnCollapse from "../../btnCollapse/BtnCollapse";
import BootstrapTable from 'react-bootstrap-table-next';
import Expanded from "../../expandedSection/Expanded";
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import ToolkitProvider, {Search} from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import "./FormCrud.css";
import {useLocation} from "react-router-dom";
import APICall from "../../../function/APICall";
import Loader from "../../loader/Loader";
import BtnGrpDelete from "../../btnGrpDelete/BtnGrpDelete";

// pour utiliser le CRUD : on accepte en props un tableau associatifs ressemblant à ça :
// const values = [
//     {"dataField": "id", "text": "Référence"},
//     {"dataField": "name", "text": "Nom"},
//     {"dataField": "city", "text": "Ville"},
// ];
// avec 3 colonnes, et 2 éléments par colonnes,
// le dataField (champ visée dans nos data )
// et text, qui va être le texte affiché en haut de nos colonnes.
function FormCrud(props) {
    // On tiendras à jour un tableau des éléments séléctionner pour utiliser ce même tableau dans les actions de groupes
    const [arraySelectedObject, setArraySelectedObject] = useState([]);

    const [products, setProducts] = useState([]);

    const location = useLocation();
    const type = props.type;
    const dataInfo = props.dataInfo;
    const defaultSorted = props.defaultSort;

    // Fonction asynchrone pour récupérer les données souhaités grâce aux props (acteur, gear, category, affectation, site).
    const callAPI = async () => {


        await APICall('/' + props.type, "GET")
            .then(res => res.json()).then(data => {
                var array = [];
                var object;
                for (const property of data.data) {
                    object = property.attributes;
                    object.id = property.id;
                    array.push(object);
                }
                setProducts(array);
            })
            .catch(() => {
                console.error();
            });

    }

    // Nos colonnes sont ici, voir comment faire quelque chose de dynamique avec les colonnes pour le moment
    var columns = [];
    dataInfo.forEach(
        element => columns.push(
            element
        )
    )
    // On ajoute à notre crud une colonne avec les bouttons d'accès aux statistiques & les boutons de délétions
    if (props.type == "affectation") {
        columns.push(
            {
                dataField: '',
                isDummyField: true,
                text: 'Actions',
                editable: false,
                formatter: (cellContent, row) => (
                    <div className={"divStyleTest"}>
                        <BtnDelete idCible={row.id} type={props.type} handle={handleDelRow}/>
                    </div>
                )
            }
        );
    } else {
        columns.push(
            {
                dataField: '',
                isDummyField: true,
                text: 'Actions',
                editable: false,
                formatter: (cellContent, row) => (
                    <div className={"divStyleTest"}>
                        <BtnStats categorieCible={location.pathname} idCible={row.id} obj={row}/>
                        <BtnDelete idCible={row.id} type={props.type} handle={handleDelRow}/>
                    </div>
                )
            }
        );
    }


    // on déclare notre barre de recherche
    const {SearchBar} = Search;

    // on permet la selection des lignes
    const selectRow = {
        mode: 'checkbox',
        clickToSelect: false,
        clickToEdit: false,
        // Lorsqu'on séléctionne/déselectionne un élément on le met ou non dans le tableau qu'on tiens à jour
        // au fur & à mesure de l'utilisation de la page
        onSelect: (row, isSelect, rowIndex, e) => {
            var index = null;
            if (isSelect) {
                arraySelectedObject.push(row.id);
            } else {
                index = arraySelectedObject.indexOf(row.id);
                if (index || index === 0) {
                    arraySelectedObject.splice(index, 1);
                }
            }
        },
        // Lorsqu'on séléctionne/déselectionne tous les éléments on les mets ou non dans le tableau qu'on tiens à jour
        // au fur & à mesure de l'utilisation de la page
        onSelectAll: (isSelect, rows, e) => {
            var index = null;
            if (isSelect) {
                for (const elem of rows) {
                    arraySelectedObject.push(elem.id);
                }
            } else {
                for (const elem of rows) {
                    index = arraySelectedObject.indexOf(elem.id);
                    if (index || index === 0) {
                        arraySelectedObject.splice(index, 1);
                    }
                }
            }
        }

    };

    // Handle row permet de changer l'objet qu'on utilise actuellement dans la page de CRUD,
    // On copie notre élément qui est immutable, puis on copie (deep copy). (!= de shallow copy)
    // Pour la modification d'élément
    // On met également à jours notre composant.
    function handleRow(row) {
        let countRow = 0
        let newProduct = JSON.parse(JSON.stringify(products));
        for (const rowOld of newProduct) {
            if (rowOld.id === row.id) {
                break;
            }
            countRow++;
        }
        newProduct[countRow] = row;
        setProducts(JSON.parse(JSON.stringify(newProduct)));
    }

    // Handle Del row permet de changer l'objet qu'on utilise actuellement dans la page de CRUD,
    // On copie notre élément qui est immutable, puis on copie (deep copy). (!= de shallow copy)
    // Pour la suppression d'éléments
    // On met également à jours notre composant.
    function handleDelRow(idCible) {
        let countRow = 0
        let newProduct = JSON.parse(JSON.stringify(products));
        for (const rowOld of newProduct) {
            if (rowOld.id === idCible) {
                break;
            }
            countRow++;
        }
        newProduct.splice(countRow, 1);
        setProducts(JSON.parse(JSON.stringify(newProduct)));
        window.location.reload(false);
    }

    // on declare et assigne tout ce dont à besoins pour nos expands, on appel l'expand générique, qui s'occupera
    // lui d'appeler l'expand spécifique en fonction du type qu'on lui passe en props
    const expandRow = {
        // On met les options que la librairie utilisée demande, à savoir ici, le fait qu'il y a une seule
        // colonne d'ouverte, et qu'on peux uniquement expand par le bouton correspondant à l'expand
        // et enfin la position de la colonne d'expand dans notre CRUD
        onlyOneExpanding: true,
        expandByColumnOnly: true,
        expandColumnPosition: 'right',

        renderer: row => (
            <div>
                <Expanded type={props.type} values={row} handle={handleRow}/>
            </div>
        ),
        showExpandColumn: true,

        // on renvois null pour ne pas avoir d'indicateur d'expand pour toutes les lignes à la fois
        expandHeaderColumnRenderer: ({isAnyExpands}) =>
            null,

        // options d'expand row, ici on modifie l'indicateur d'expand des row
        expandColumnRenderer: ({expanded}) => {
            return (
                <BtnCollapse/>
            );
        }
    };

    // option pagination (message de la taille de la pagination)
    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total">
                {from} à {to} sur {size} résultats
            </span>
    );

    // retour du nombre d'élément à afficher sur l'écran -> pour avoir à peut près toujours le même nombre
    // d'éléments affichés sur la page
    const numberElemToDisplay = () => {
        var winScreenY = window.screen.height;
        const margeTopBarSimple = 75;
        const margeBottom = 20;
        const sizeRow = 80;
        const search = 60;
        const pagination = 60;
        const topCrud = 60;

        var expand = 460;
        switch (props.type) {
            case 'acteur':
                expand = 200;
                break;
            case 'site':
                expand = 400;
                break;
            case 'category':
                expand = 400;
                break;
            case 'gear':
                expand = 500;
                break;
            case 'affectation':
                expand = 500;
                break;
        }

        var result = ~~((winScreenY * 2 - margeTopBarSimple - margeBottom - expand - pagination - search - topCrud) / sizeRow)
        if (result) {
            return result
        } else {
            return 1
        }
    }

    // options de la pagination
    const options = {
        sizePerPage: numberElemToDisplay(),
        paginationSize: 4,
        pageStartIndex: 0,
        hideSizePerPage: true,
        showTotal: true,
        paginationTotalRenderer: customTotal,
        disablePageTitle: true,
    };

    // Loader custom pour les chargements du crud
    const NoDataIndication = () => (
        <Loader/>
    );

    useEffect(() => {
        callAPI()
    }, []);

    return (
        <div>
            <ToolkitProvider
                keyField="id"
                data={products}
                columns={columns}
                search
            >
                {
                    props => (
                        <div className={"crud"}>
                            <div className={"mb-2"}>
                                <BtnGrpDelete type={type} arrayToDelete={arraySelectedObject} handle={handleDelRow}/>
                                <SearchBar {...props.searchProps} placeholder="Recherche"/>
                            </div>
                            <BootstrapTable {...props.baseProps} keyField='id' data={products} columns={columns}
                                            defaultSorted={defaultSorted}
                                            expandRow={expandRow}
                                            bordered={false}
                                            pagination={paginationFactory(options)}
                                            selectRow={selectRow}
                                            noDataIndication={() => <NoDataIndication/>}
                            />
                        </div>
                    )
                }
            </ToolkitProvider>
        </div>
    )
}

export default FormCrud;