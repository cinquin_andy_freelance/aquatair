import React, {useEffect, useState} from 'react';
import '../AddForm.css';
import {useForm} from "react-hook-form";
import {useHistory} from "react-router-dom";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import btnAddSvg from '../../../ressources/icons/btnAdd.svg';
import btnCancel from '../../../ressources/icons/btnCancel.svg';
import APICall from "../../../function/APICall";
import moment from "moment";
import StringFormat from "../../../function/StringFormat";

// Schéma qui doit être respecté pour la validation du formulaire sinon il ne pourra pas être submit.
const schema = yup.object().shape({
    shortId: yup.string().required('Veuillez entrer la référence'),
    name: yup.string().required('Veuillez entrer un nom'),
    type: yup.string(),
    mark: yup.string(),
    usury: yup.string(),
    purchaseDate: yup.date().typeError('Entrez une date valide : jj/mm/aaaa').required('Veuillez entrer la date d\'obtention'),
    endLifeDate: yup.date().nullable().transform((curr, orig) => orig === '' ? null : curr).typeError('Entrez une date valide : jj/mm/aaaa'),
    description: yup.string(),
    categoryId: yup.string().required('Veuillez sélectionner une catégorie'),
})

function AddGearForm() {
    // State pour gérer le retour utilisateur si l'ajout des données c'est bien passé ou non.
    const [message, setMessage] = useState('');

    // State où l'on récupère la catégorie sélectionnée.
    const [category, setCategory] = useState('');

    // State où on récupère toutes les catégorires qui existent en base de données, pour pouvoir les sélectionner sur la page d'ajout d'équipement.
    const [array, setArray] = useState([]);

    const changeState = (message) => {
        setMessage(message);
    }

    const handleChange = (event) => {
        setCategory(event.target.value)
    }

    // Fonction asynchrone pour récupérer "GET" toutes les catégories présentes en base de données.
    const loadCategories = async () => {
        await APICall('/category', 'GET')
            .then(res => res.json())
            .then((res) => {
                showCategories(res.data);
            }).catch(() => {
                console.error();
            })
    }

    // Fonction utilisé pour ne pas boucler indéfiniment sur la fonction loadCategories.
    useEffect(() => {
        loadCategories();
        // eslint-disable-next-line
    }, [])

    // Fonction pour créer les options du select des catégories.
    function showCategories(data) {
        let toShow = [];
        for (const el of data) {
            toShow.push(<option key={el.id} value={el.id}>Référence : {el.attributes.shortId}; Nom
                : {el.attributes.name}</option>);
        }
        setArray(toShow);
    }

    const {register, handleSubmit, formState, errors, reset} =
        useForm({
            mode: 'onTouched',
            resolver: yupResolver(schema),
        });

    const {isSubmitting} = formState;

    const history = useHistory();

    // Fonction pour formater les données avant de les envoyés "POST" en base de données.
    const onSubmit = async data => {
        const dataFormat = {
            "data": {
                "attributes": {
                    "shortId": data.shortId.toUpperCase(),
                    "name": StringFormat(data.name),
                    "gearType": StringFormat(data.type),
                    "mark": StringFormat(data.mark),
                    "usury": StringFormat(data.usury),
                    "purchaseDate": moment(data.purchaseDate).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                    "endLifeDate": moment(data.endLifeDate).format("YYYY-MM-DD HH:mm:ss.SSSSSS"),
                    "description": data.description,
                    "categoryId": category
                },
                "type": "Gear"
            }
        };
        if (dataFormat.data.attributes.purchaseDate === "Invalid date") {
            dataFormat.data.attributes.purchaseDate = moment.now();
        }
        if (dataFormat.data.attributes.endLifeDate === "Invalid date") {
            dataFormat.data.attributes.endLifeDate = null;
        }
        await postNewGear(dataFormat);
        reset();
    }

    // Fonction pour envoyés les données formatées en base de données.
    const postNewGear = async (data) => {
        await APICall("/gear", "POST", data)
            .then((res) => {
                if (res.status === 201) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000));

                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000));

                }
            })
            .catch(() => {
                console.error();
            });
    }

    return (
        <form className="container py-5" onSubmit={handleSubmit(onSubmit)}>
            {message}
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="shortId">Référence</label>
                    <input type="text" className="form-control" id="shortId"
                           placeholder="Référence (Simple)" name="shortId"
                           ref={register}/>
                    {errors.shortId && <span>{errors.shortId.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="name">Nom</label>
                    <input type="text" className="form-control" id="name"
                           placeholder="Nom" name="name"
                           ref={register}/>
                    {errors.name && <span>{errors.name.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="type">Type</label>
                    <input type="text" className="form-control" id="type"
                           placeholder="Type" name="type"
                           ref={register}/>
                    {errors.type && <span>{errors.type.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="mark">Marque</label>
                    <input type="text" className="form-control" id="mark"
                           placeholder="Marque" name="mark"
                           ref={register}/>
                    {errors.mark && <span>{errors.mark.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="usury">Usure</label>
                    <input type="text" className="form-control" id="usury"
                           placeholder="État d'usure" name="usury"
                           ref={register}/>
                    {errors.usury && <span>{errors.usury.message}</span>}
                </div>
            </div>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="purchaseDate">Date d'obtention</label>
                    <input type="date" className="form-control" id="purchaseDate"
                           placeholder="Date : JJ/MM/YYYY" name="purchaseDate"
                           ref={register}/>
                    {errors.purchaseDate && <span>{errors.purchaseDate.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="endLifeDate">Date de rendu</label>
                    <input type="date" className="form-control" id="endLifeDate"
                           placeholder="Date : JJ/MM/YYYY" name="endLifeDate"
                           ref={register}/>
                    {errors.endLifeDate && <span>{errors.endLifeDate.message}</span>}
                </div>
            </div>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="description">Description</label>
                    <textarea className="form-control" id="description"
                              placeholder="Description" name="description"
                              ref={register}/>
                    {errors.description && <span>{errors.description.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="categoryId">Catégorie liée</label>
                    <select value={category} onChange={handleChange} className="form-control" name="categoryId"
                            id="categoryId" ref={register}>
                        <option value="NO-CAT">Sélectionnez une catégorie</option>
                        {array}
                    </select>
                    {errors.categoryId && <span>{errors.categoryId.message}</span>}
                </div>
            </div>
            <div className="button-group">
                <button disabled={isSubmitting} type="button" onClick={() => {
                    history.goBack()
                }} className="btn btn-cancel">
                    <img src={btnCancel} alt="icones bouton retour"/>
                    Annuler
                </button>
                <button disabled={isSubmitting} type="submit" className="btn btn-add">
                    <img src={btnAddSvg} alt="icones bouton ajout"/>
                    Ajouter
                </button>
            </div>
        </form>
    )
}

export default AddGearForm;