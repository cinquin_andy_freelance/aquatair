import React, {useState} from 'react';
import '../AddForm.css';
import {useForm} from "react-hook-form";
import {useHistory} from "react-router-dom";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import btnAddSvg from '../../../ressources/icons/btnAdd.svg';
import btnCancel from '../../../ressources/icons/btnCancel.svg';
import APICall from "../../../function/APICall";
import StringFormat from "../../../function/StringFormat";
import {AsYouType} from 'libphonenumber-js'

// Regex qui permet de vérifier si le numéro entré dans l'input phoneNumber est conforme.
// const phoneRegex = /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,3})|(\(?\d{2,3}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/;
// const phoneRegex = /^(?:(?:\+|00)[0-9]{,2}[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;
// const phoneRegex = /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/gm;

// Schéma qui doit être respecté pour la validation du formulaire sinon il ne pourra pas être submit.
const schema = yup.object().shape({
    lastname: yup.string().required('Veuillez entrer un nom'),
    firstname: yup.string().required('Veuillez entrer un prénom'),
    phoneNumber: yup.string().required('Veuillez entrer un numéro de téléphone'),
    // phoneNumber: yup.string().matches(phoneRegex, 'Numéro non conforme, ex : 06 21 58 26 84 ou +33 6 21 58 26 84').required(),
})

function AddPersonForm() {
    // State pour gérer le retour utilisateur si l'ajout des données c'est bien passé ou non.

    const formatPhoneNumber = (e) => {
        e.target.value = new AsYouType('FR').input(e.target.value);
    }

    const [message, setMessage] = useState('');

    const changeState = (message) => {
        setMessage(message);
    }

    const {register, handleSubmit, formState, errors, reset} =
        useForm({
            mode: 'onTouched',
            resolver: yupResolver(schema),
        });

    const {isSubmitting} = formState;

    // useHistory() utilisé pour le bouton retour, pour retrouver l'avant dernière page visitée.
    const history = useHistory();

    // Fonction pour formater les données avant de les envoyés "POST" en base de données.
    const onSubmit = async data => {
        const dataFormat = {
            "data": {
                "attributes": {
                    "firstname": StringFormat(data.firstname),
                    "lastname": data.lastname.toUpperCase(),
                    "phoneNumber": data.phoneNumber
                },
                "type": "Acteur"
            }
        };
        await postNewPeople(dataFormat);
        reset();
    }

    // Fonction pour envoyés les données formatées en base de données.
    const postNewPeople = async (data) => {
        await APICall("/acteur", "POST", data)
            .then((res) => {
                if (res.status === 201) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000));

                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000));

                }
            })
            .catch(() => {
                console.error();
            });
    }

    return (
        <form className="container py-5" onSubmit={handleSubmit(onSubmit)}>
            {message}
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="lastname">Nom</label>
                    <input type="text" className="form-control" id="lastname"
                           placeholder="Nom" name="lastname"
                           ref={register}/>
                    {errors.lastname && <span>{errors.lastname.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="firstname">Prénom</label>
                    <input type="text" className="form-control" id="firstname"
                           placeholder="Prénom" name="firstname"
                           ref={register}/>
                    {errors.firstname && <span>{errors.firstname.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="phoneNumber">Numéro de téléphone</label>
                    <input type="text" className="form-control" id="phoneNumber"
                           placeholder="0123456789 ou +33 0123456789" name="phoneNumber"
                           ref={register} onChange={formatPhoneNumber}/>
                    {errors.phoneNumber && <span>{errors.phoneNumber.message}</span>}
                </div>
            </div>
            <div className="button-group">
                <button disabled={isSubmitting} type="button" onClick={() => {
                    history.goBack()
                }} className="btn btn-cancel">
                    <img src={btnCancel} alt="icones bouton retour"/>
                    Annuler
                </button>
                <button disabled={isSubmitting} type="submit" className="btn btn-add">
                    <img src={btnAddSvg} alt="icones bouton ajout"/>
                    Ajouter
                </button>
            </div>
        </form>
    )
}

export default AddPersonForm;