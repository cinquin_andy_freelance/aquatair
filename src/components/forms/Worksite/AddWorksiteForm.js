import React, {useState} from 'react';
import '../AddForm.css';
import {useForm} from "react-hook-form";
import {useHistory} from "react-router-dom";
import {yupResolver} from '@hookform/resolvers/yup';
import * as yup from "yup";
import btnAddSvg from '../../../ressources/icons/btnAdd.svg';
import btnCancel from '../../../ressources/icons/btnCancel.svg';
import APICall from "../../../function/APICall";
import StringFormat from "../../../function/StringFormat";

// Schéma qui doit être respecté pour la validation du formulaire sinon il ne pourra pas être submit.
const schema = yup.object().shape({
    name: yup.string().required('Veuillez entrer un nom'),
    city: yup.string().required('Veuillez entrer une ville'),
    address: yup.string(),
    description: yup.string(),
})

function AddWorksiteForm() {
    // State pour gérer le retour utilisateur si l'ajout des données c'est bien passé ou non.
    const [message, setMessage] = useState('');

    const changeState = (message) => {
        setMessage(message);
    }

    const {register, handleSubmit, formState, errors, reset} =
        useForm({
            mode: 'onTouched',
            resolver: yupResolver(schema),
        });

    const {isSubmitting} = formState;

    // useHistory() utilisé pour le bouton retour, pour retrouver l'avant dernière page visitée.
    const history = useHistory();

    // Fonction pour formater les données avant de les envoyés "POST" en base de données.
    const onSubmit = async data => {
        const dataFormat = {
            "data": {
                "attributes": {
                    "name": StringFormat(data.name),
                    "city": StringFormat(data.city.toUpperCase()),
                    "address": data.address,
                    "description": data.description
                },
                "type": "Site"
            }
        };
        await postNewWorksite(dataFormat);
        reset();
    }

    // Fonction pour envoyés les données formatées en base de données.
    const postNewWorksite = async (data) => {
        await APICall("/site", "POST", data)
            .then((res) => {
                if (res.status === 201) {
                    changeState(<div className="alert alert-success">Bien
                        enregistrée</div>, setTimeout(() => changeState(''), 3000));

                } else {
                    changeState(<div className="alert alert-danger">Une erreur c'est produite
                    </div>, setTimeout(() => changeState(''), 3000));

                }
            })
            .catch(() => {
                console.error();
            });
    }

    return (
        <form className="container py-5" onSubmit={handleSubmit(onSubmit)}>
            {message}
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="name">Nom</label>
                    <input type="text" className="form-control" id="name"
                           placeholder="Nom" name="name"
                           ref={register}/>
                    {errors.name && <span>{errors.name.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="city">Ville</label>
                    <input type="text" className="form-control" id="city"
                           placeholder="Ville" name="city"
                           ref={register}/>
                    {errors.city && <span>{errors.city.message}</span>}
                </div>
            </div>
            <div className="row">
                <div className="col-sm form-group">
                    <label htmlFor="address">Adresse</label>
                    <textarea className="form-control" id="address"
                              placeholder="Adresse" name="address"
                              ref={register}/>
                    {errors.address && <span>{errors.address.message}</span>}
                </div>
                <div className="col-sm form-group">
                    <label htmlFor="description">Description</label>
                    <textarea className="form-control" id="description"
                              placeholder="Description" name="description"
                              ref={register}/>
                    {errors.description && <span>{errors.description.message}</span>}
                </div>
            </div>
            <div className="button-group">
                <button disabled={isSubmitting} type="button" onClick={() => {
                    history.goBack()
                }} className="btn btn-cancel">
                    <img src={btnCancel} alt="icones bouton retour"/>
                    Annuler
                </button>
                <button disabled={isSubmitting} type="submit" className="btn btn-add">
                    <img src={btnAddSvg} alt="icones bouton ajout"/>
                    Ajouter
                </button>
            </div>
        </form>
    )
}

export default AddWorksiteForm;