import React from "react";
import './Btn.css';
import {Link} from "react-router-dom";

function Btn({path, svg, text, buttonType}) {
    return (
        <Link to={path} className={buttonType}>
            <img src={svg} alt={svg}/>
            {text}
        </Link>
    );
}

export default Btn;