import React from "react";
import './Loader.css';

// Loader perso -> logo aquatair qui se 'dessine' en boucle.
function Loader() {
    return (
        <div className="container LoaderIcons">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px"
                 y="0px"
                 viewBox="0 0 1000 1000" className="h-75">
                <g className="LoaderIcon" id="LoaderIcon-Calque_1">
                    <path className="LoaderIcon-st0" d="M802,706.9C755.7,736.6,678,771.1,580,771.1c-26.7,0-52.5-3-77.2-8.7c0,0,0,0-0.1,0c-0.3-0.1-0.6-0.1-0.8-0.2
                    c-0.1,0-0.2,0-0.3-0.1c0,0,0,0,0,0C370.3,731.6,273.1,626,271.2,499.8c1.9-126.1,99.1-231.7,230.5-262.3c0,0,0,0,0,0
                    c0.1,0,0.2,0,0.3-0.1c0.3-0.1,0.5-0.1,0.8-0.2c0,0,0,0,0.1,0c24.7-5.6,50.5-8.7,77.2-8.7c98,0,177.5,35.5,223.8,65.3
                    c75.1,48.2-90.4-134.1-301-134.1c-211.9,0-383.8,151.9-386.1,340l0,0c0,0,0,0,0,0s0,0,0,0l0,0c2.3,188.1,174.3,340,386.1,340
                    C713.4,839.8,877,658.7,802,706.9z"/>
                </g>
                <g className="LoaderIcon" id="LoaderIcon-Calque_2">
                    <path className="LoaderIcon-st1" d="M586,238.8c-29.1,0-57.4,3-84.3,8.5c144.2,29.4,250.5,131.5,250.5,253.1S645.8,724.2,501.7,753.5
            c26.9,5.5,55.2,8.5,84.3,8.5c183.6,0,296.6-117.7,296.6-262.2C882.6,355.4,769.6,238.8,586,238.8z"/>
                </g>
            </svg>

        </div>
    );
}

export default Loader;