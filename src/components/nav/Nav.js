import React from "react";
import {Link} from "react-router-dom";
import './Nav.css';
import AquatairSvg from '../../ressources/icons/aquatair.svg';
import assignSvg from '../../ressources/icons/tool-box.svg';
import categorySvg from '../../ressources/icons/folderWhite.svg';
import worksiteSvg from '../../ressources/icons/constructionWhite.svg';
import userSvg from '../../ressources/icons/userWhite.svg';
import gearSvg from '../../ressources/icons/wrenchWhite.svg';
import logOut from '../../ressources/icons/logout.svg';
import Cookies from 'js-cookie';
import Downloader from "../downloader/Downloader";

function LogOut() {
    Cookies.set('token', '');
    window.location = '/login';
}

//Composant fonctionnel pour la bar de navigation présent sur toutes les pages.
function Nav() {
    return (
        <nav>
            <ul>
                <Link to="/">
                    <li><img src={AquatairSvg} alt="aquatair.svg"/></li>
                </Link>
                <Link to="/personnes">
                    <li><img src={userSvg} alt="user.svg"/></li>
                </Link>
                <Link to="/chantiers">
                    <li><img src={worksiteSvg} alt="worksite.svg"/></li>
                </Link>
                <Link to="/categories">
                    <li><img src={categorySvg} alt="category.svg"/></li>
                </Link>
                <Link to="/equipements">
                    <li><img src={gearSvg} alt="gear.svg"/></li>
                </Link>
                <Link to="/affectations">
                    <li><img src={assignSvg} alt="assign.svg"/></li>
                </Link>
                <Downloader/>
                <button onClick={() => {
                    LogOut()
                }}>
                    <li>
                        <img src={logOut} alt="deconnexion"/>
                    </li>
                </button>
            </ul>
        </nav>
    );
}

export default Nav;