import React from "react";
import './TopBarSimple.css';
import Btn from "../homePageButton/Btn";
import btnAddSvg from "../../ressources/icons/btnAdd.svg";
import btnListSvg from "../../ressources/icons/btnList.svg";
import {useLocation} from "react-router-dom";


// Composant fonctionnel pour la barre du haut des pages, avec une fonction permettant de déterminer quelles boutons
// sont censés s'afficher en fonction de la page sur laquelle l'utilisateur se trouve.
function TopBarSimple({title, from}) {
    const location = useLocation();

    const displayValue = () => {
        let call;
        if (location.pathname.includes("personnes")) {
            call = "personnes";
        } else if (location.pathname.includes("chantiers")) {
            call = "chantiers";
        } else if (location.pathname.includes("categories")) {
            call = "categories";
        } else if (location.pathname.includes("equipements")) {
            call = "equipements";
        } else if (location.pathname.includes("affectations")) {
            call = "affectations";
        }
        // On affiche en fonction de notre position actuelle, l'accès à la page de liste / ajout
        if (location.pathname.includes("-ajouter")) {
            return <Btn path={`/${call}`} svg={btnListSvg} text="Voir liste" buttonType="button button-list"/>
        } else {
            return <Btn path={`/${call}-ajouter`} svg={btnAddSvg} text="Ajouter" buttonType="button button-add"/>
        }
    }

    return (
        <div className="top-bar">
            <h1>
                {title} <span>{from}</span>
            </h1>
            <div className={"btnContain"}>
                {displayValue()}
            </div>
        </div>
    );
}

export default TopBarSimple;