import cookies from 'js-cookie';

function APICall(url, methode, body = null) {

    // On construit notre requete avec les bons éléments, notamment le header de la requete.
    let obj = {
        method: methode,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(cookies.get('username') + ':' + cookies.get('token')),
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
            'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH'
        },
    }

    if (body !== null) {
        obj = {...obj, body: JSON.stringify(body)}
    }

    // Requete en local --- return fetch('http://localhost:5000' + url, obj
    return fetch('http://dev.andy-cinquin.fr:5000' + url, obj
    );

}

export default APICall;