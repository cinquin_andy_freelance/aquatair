function StringFormat(stringCible) {
    // Formatage de la chaine cible , pour avoir une majuscule sur le premier mot & le reste en minuscule.
    if (stringCible !== undefined) {
        let result = stringCible.toLowerCase();
        result = (result + '').charAt(0).toUpperCase() + result.substr(1);
        return result;
    }
}

export default StringFormat;