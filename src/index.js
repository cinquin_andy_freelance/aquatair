import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Cookies from 'js-cookie';
import './index.css';
import Nav from "./components/nav/Nav";
import Home from "./pages/Home/Home";
import Employee from "./pages/Employee/Employee";
import EmployeeStats from "./pages/Employee/EmployeeStats";
import Worksite from "./pages/Worksite/Worksite";
import WorksiteStats from "./pages/Worksite/WorksiteStats";
import Gear from "./pages/Gear/Gear";
import GearStats from "./pages/Gear/GearStats";
import Category from "./pages/Category/Category";
import CategoryStats from "./pages/Category/CategoryStats";
import Assign from "./pages/Assign/Assign";
import NotFound from "./pages/NotFound";
import AddPersonPage from "./pages/Employee/AddPersonPage";
import AddWorksitePage from "./pages/Worksite/AddWorksitePage";
import AddCategoryPage from "./pages/Category/AddCategoryPage";
import AddGearPage from "./pages/Gear/AddGearPage";
import AddAssignPage from "./pages/Assign/AddAssignPage";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Login from "./pages/Login/Login";
import APICall from "./function/APICall";


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            token: Cookies.get('token'),
            nav: window.location.pathname !== '/login',
            allData: [],
            gearData: [],
            categoryData: [],
            assignData: [],
            authorData: [],
            siteData: [],
        };
        this.checkToken();
    }

    // regarder si le token est valid par rapport à la base de donnée, pour permettre la connexion
    checkToken() {
        // check if token in the cookies is valid.
        if (Cookies.get('username') === undefined) {
            Cookies.set('username', '');
            Cookies.set('token', '');
            window.location = '/login';
        } else if (Cookies.get('token') === undefined) {
            Cookies.set('token', '');
            window.location = '/login';
        } else {
            APICall('/user/checkToken?username=' + Cookies.get('username') + '&token=' + Cookies.get('token'), 'GET').then(res => res.json()).then(data => {
                    if (data.meta.result[0] !== 'ok') {
                        if (window.location.pathname === '/login') {
                            return true;
                        }
                        Cookies.set('username', '');
                        Cookies.set('token', '');
                        window.location = '/login';
                    } else {
                        if (window.location.pathname === '/login') {
                            window.location = '/';
                        }
                    }
                }
            )
        }
    }

    render() {
        return (
            <Router>
                <div className="App">
                    {this.state.nav && <Nav/>}
                    <Switch>
                        {/*On met exact car sinon la home page va se charger à chaque fois et empêcher les autres d'être chargé*/}
                        <Route path="/" exact component={Home}/>
                        <Route path="/affectations/:id"
                               component={(routeurProps) => <GearStats idCible={routeurProps.match.params.id}/>}/>
                        <Route path="/affectations" component={Assign}/>
                        <Route path="/login" component={Login}/>
                        <Route path="/personnes/:id"
                               component={(routeurProps) => <EmployeeStats idCible={routeurProps.match.params.id}/>}/>
                        <Route path="/personnes" component={Employee}/>
                        <Route path="/equipements/:id"
                               component={(routeurProps) => <GearStats idCible={routeurProps.match.params.id}/>}/>
                        <Route path="/equipements" component={Gear}/>
                        <Route path="/chantiers/:id"
                               component={(routeurProps) => <WorksiteStats idCible={routeurProps.match.params.id}/>}/>
                        <Route path="/chantiers" component={Worksite}/>
                        <Route path="/categories/:id"
                               component={(routeurProps) => <CategoryStats idCible={routeurProps.match.params.id}/>}/>
                        <Route path="/categories" component={Category}/>
                        <Route path="/personnes-ajouter" component={AddPersonPage}/>
                        <Route path="/chantiers-ajouter" component={AddWorksitePage}/>
                        <Route path="/categories-ajouter" component={AddCategoryPage}/>
                        <Route path="/equipements-ajouter" component={AddGearPage}/>
                        <Route path="/affectations-ajouter" component={AddAssignPage}/>
                        <Route component={NotFound}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

ReactDOM.render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>,
    document.getElementById('root')
);

