import React from "react";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import AddAssignForm from "../../components/forms/Assign/AddAssignForm";


// Composant correspondant à la page d'ajout des affectations.
class AddAssignPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="Page">
                <TopBarSimple title="Nouvelle affectation d'" from="équipement"/>
                {/*Appel du composant formulaire pour l'ajout d'affectation*/}
                <AddAssignForm/>
            </div>
        );
    };
}

export default AddAssignPage;