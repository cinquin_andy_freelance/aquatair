import React, {useEffect, useState} from "react";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import FormCrud from "../../components/forms/Crud/FormCrud";
import APICall from "../../function/APICall";
import moment from "moment";
import 'moment/locale/fr';

// On met formate nos dates en français.
moment.locale('fr');

// Permet de créer une librairie pour les catégories prenant comme informations leur ID ainsi que leur shortId.
function CategoryFormater(LibraryCategory, setLibraryCategory) {
    return APICall('/category', 'GET').then(res => res.json().then(data => {
            var newMap = new Map(LibraryCategory)
            for (const cat of data.data) {
                newMap.set(cat.id, cat.attributes.shortId);
            }
            setLibraryCategory(newMap);
        }
    ))
}

// Permet de créer une librairie pour les équipements prenant comme informations leur ID ainsi que leur shortId.
function CategoryGearFormater(LibraryCategoryGear, setLibraryCategoryGear) {
    return APICall('/gear', 'GET').then(res => res.json().then(data => {
            var newMap = new Map(LibraryCategoryGear)
            for (const cat of data.data) {
                newMap.set(cat.id, cat.attributes.categoryId);
            }
            setLibraryCategoryGear(newMap);
        }
    ))
}

// Permet de créer une librairie pour les équipements prenant comme informations leur ID ainsi que leur shortId.
//TODO check si c'est dupliqué avec la fonction du dessus ?
function GearFormater(LibraryGear, setLibraryGear) {
    return APICall('/gear', 'GET').then(res => res.json().then(data => {
            var newMap = new Map(LibraryGear)
            for (const cat of data.data) {
                newMap.set(cat.id, cat.attributes.shortId);
            }
            setLibraryGear(newMap);
        }
    ))
}

// Permet de créer une librairie pour les chantiers prenant comme informations leur ID ainsi que leur nom.
function SiteFormater(LibrarySite, setLibrarySite) {
    return APICall('/site', 'GET').then(res => res.json().then(data => {
            var newMap = new Map(LibrarySite)
            for (const cat of data.data) {
                newMap.set(cat.id, cat.attributes.name);
            }
            setLibrarySite(newMap);
        }
    ))
}

// Permet de créer une librairie pour les acteurs prenant comme informations leur ID ainsi que leur prénom et nom.
function ActeurFormater(LibraryActeur, setLibraryActeur) {
    return APICall('/acteur', 'GET').then(res => res.json().then(data => {
            var newMap = new Map(LibraryActeur)
            for (const cat of data.data) {
                newMap.set(cat.id, cat.attributes.firstname + " " + cat.attributes.lastname);
            }
            setLibraryActeur(newMap);
        }
    ))
}

function Assign() {

    // Déclaration des "States" utilisés pour notre composant fonctionnel Assign (pour la page des affectations).
    const [LibraryCategory, setLibraryCategory] = useState(new Map());
    const [LibraryGearCategory, setLibraryGearCategory] = useState(new Map());
    const [LibraryGear, setLibraryGear] = useState(new Map());
    const [LibrarySite, setLibrarySite] = useState(new Map());
    const [LibraryActeur, setLibraryActeur] = useState(new Map());

    // Permet d'afficher la bonne information afin de savoir si l'équipement a été réservé pour un acteur ou un chantier.
    function DisplayedValueAssignement(siteId, acteurId) {
        var displayedValue = "";
        if (siteId === "none" || siteId === null) {
            displayedValue = "Acteur : ";
            displayedValue += LibraryActeur.get(acteurId);
        } else {
            displayedValue = "Chantier : ";
            displayedValue += LibrarySite.get(siteId);
        }
        return displayedValue
    }

    function DisplayedValueReference(gearId) {
        for (const elem of LibraryGearCategory) {
            if (elem[0] === gearId) {
                return LibraryCategory.get(elem[1]) + "_" + LibraryGear.get(gearId);
            }
        }
    }

    function DisplayedValueDate(date) {
        if (date === null) {
            return "";
        } else {
            date = moment(date);
        }
        return date.format("dddd DD MMMM");
    }

    // On récupère toutes les valeurs de tous nos éléments et on fait tous nos appels à l'API.
    useEffect(() => {
        CategoryFormater(LibraryCategory, setLibraryCategory);
        CategoryGearFormater(LibraryGearCategory, setLibraryGearCategory);
        GearFormater(LibraryGear, setLibraryGear);
        SiteFormater(LibrarySite, setLibrarySite);
        ActeurFormater(LibraryActeur, setLibraryActeur);
    }, []);

    if (LibraryCategory.size !== 0 && LibraryGear !== 0 && LibrarySite !== 0 && LibraryActeur !== 0) {
        // Valeur à afficher dans le crud, c'est ici qu'on gère la façon d'afficher les données,
        // et les champs à séléctionner dans le crud.
        const values = [
            {
                "dataField": "id",
                "text": "Référence",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {`${DisplayedValueReference(row.gearId)}`}
                    </div>
                ),
                sortValue: (cell, row) => (
                    `${DisplayedValueReference(row.gearId)}`
                ),
                filterValue: (cell, row) => (
                    `${DisplayedValueReference(row.gearId)}`
                )
            },
            {
                "dataField": "assign",
                "text": "Assigné à",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {`${DisplayedValueAssignement(row.siteId, row.acteurId)}`}
                    </div>
                ),
                sortValue: (cell, row) => (
                    `${DisplayedValueAssignement(row.siteId, row.acteurId)}`
                ),
                filterValue: (cell, row) => (
                    `${DisplayedValueAssignement(row.siteId, row.acteurId)}`
                )
            },
            {
                "dataField": "startDateAssignation",
                "text": "Affectation",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {`${DisplayedValueDate(row.startDateAssignation)}`}
                    </div>
                )
            },
            {
                "dataField": "endDateAssignation",
                "text": "Rendu",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {`${DisplayedValueDate(row.endDateAssignation)}`}
                    </div>
                )
            },
        ];


        return (
            <div className="Page">
                <TopBarSimple title="Affectations des" from="équipements" type={"affectation"}/>
                {/* on peux passer comme 'type' en props, le type de données qu'on veux affichés, à savoir :
                    category
                    acteur
                    site
                    gear
                    affectation
                    Toutes les infos propres aux différents affichages seront alors affichés à ce moment la */}
                <FormCrud dataInfo={values} type={"affectation"}/>
            </div>
        );
    }
    return null

}

export default Assign;