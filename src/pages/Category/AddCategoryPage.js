import React from "react";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import AddCategoryForm from "../../components/forms/Category/AddCategoryForm";


// Composant correspondant à la page d'ajout des catégories.
class AddCategoryPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="Page">
                <TopBarSimple title="Ajout d'une nouvelle" from="catégorie"/>
                {/*Appel du composant formulaire pour l'ajout de catégorie*/}
                <AddCategoryForm/>
            </div>
        );
    };
}

export default AddCategoryPage;