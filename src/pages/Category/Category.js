import React from "react";

import FormCrud from "../../components/forms/Crud/FormCrud";

import TopBarSimple from "../../components/topBarSimple/TopBarSimple";

function Category() {
    // Values à afficher dans le crud, c'est ici qu'on gère la façon d'affichés les données,
    // et les champs à séléctionner dans le crud
    const values = [
        {"dataField": "shortId", "text": "Référence Simple", "sort": true},
        {"dataField": "name", "text": "Nom", "sort": true},
    ];

    const defaultSorted = [{
        dataField: "shortId",
        order: "asc"
    }]

    return (
        <div className="Page">
            <TopBarSimple title="Gestion des" from="catégories" type={"category"}/>
            {/* on peux passer comme 'type' en props, le type de données qu'on veux affichés, à savoir :
                    category
                    acteur
                    site
                    gear
                    affectation
                    Toutes les infos propres aux différents affichages seront alors affichés à ce moment la */}
            <FormCrud dataInfo={values} type={"category"} defaultSort={defaultSorted}/>
        </div>
    );
}

export default Category;