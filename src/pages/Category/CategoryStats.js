import React, {useEffect, useState} from "react";
import FormCrud from "../../components/forms/Crud/FormCrud";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import APICall from "../../function/APICall";


// Fonction de créer une librairie avec les informations des catégories en récupérant leur ID et leur shortId.
function ShortIdFormater(Library, setLibrary) {
    return APICall('/category', 'GET').then(res => res.json().then(data => {
            var newMap = new Map(Library)
            for (const cat of data.data) {
                newMap.set(cat.id, cat.attributes.shortId);
            }
            setLibrary(newMap);
        }
    ))
}

function CheckIfEmpty(idCible, setError) {
    return APICall('/gear/?filter[categoryId]=' + idCible, 'GET').then(res => res.json().then(data => {
            var array = [];
            var object;
            for (const property of data.data) {
                object = property.attributes;
                object.id = property.id;
                array.push(object);
            }
            if (array.length === 0) {
                setError("il n'y a pas de statistique pour le moment")
            }
        }
    ))
}


function CategoryStats({idCible}) {
    const [Library, setLibrary] = useState(new Map);
    const [error, setError] = useState(null);

    useEffect(() => {
        CheckIfEmpty(idCible, setError);
        ShortIdFormater(Library, setLibrary);
        // eslint-disable-next-line
    }, []);

    if (Library.size !== 0) {
        const values = [
            {
                "dataField": "shortId",
                "text": "Référence (Simple)",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {
                            `${Library.get(row.categoryId)}_${row.shortId}`
                        }
                    </div>
                ),
                sortValue: (cell, row) => (
                    `${Library.get(row.categoryId)}_${row.shortId}`
                ),
                filterValue: (cell, row) => (
                    `${Library.get(row.categoryId)}_${row.shortId}`
                ),
            },
            {"dataField": "name", "text": "Nom", "sort": true},
            {"dataField": "mark", "text": "Marque", "sort": true},
            {
                "dataField": "id",
                "text": "Référence unique",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {
                            `${(row.id).substring(0, 8)}`
                        }
                    </div>
                ),
                sortValue: (cell, row) => (
                    `${(row.id).substring(0, 8)}`
                ),
                filterValue: (cell, row) => (
                    `${(row.id).substring(0, 8)}`
                ),
            },
        ];

        return (
            <div className="Page">
                <TopBarSimple title="Gestion des" from="équipements"/>

                <FormCrud dataInfo={values} type={"gear/?filter[categoryId]=" + idCible}/>

                {error !== null &&
                <div className="login-alert">
                    <div className="container">
                        {error}
                    </div>
                </div>
                }
            </div>
        );
    } else {
        return null
    }
}

export default CategoryStats;












