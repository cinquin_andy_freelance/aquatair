import React from "react";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import AddPersonForm from "../../components/forms/Person/AddPersonForm";


// Composant correspondant à la page d'ajout des acteurs (personnes).
class AddPersonPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="Page">
                <TopBarSimple title="Ajout d'une nouvelle" from="personne"/>
                {/*Appel du composant formulaire pour l'ajout d'acteur (personne)*/}
                <AddPersonForm/>
            </div>
        );
    };
}

export default AddPersonPage;