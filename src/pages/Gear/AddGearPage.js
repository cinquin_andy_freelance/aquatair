import React from "react";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import AddGearForm from "../../components/forms/Gear/AddGearForm";


// Composant correspondant à la page d'ajout des équipements.
class AddGearPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="Page">
                <TopBarSimple title="Ajout d'un nouvel" from="outil"/>
                {/*Appel du composant formulaire pour l'ajout d'équipement*/}
                <AddGearForm/>
            </div>
        );
    };
}

export default AddGearPage;