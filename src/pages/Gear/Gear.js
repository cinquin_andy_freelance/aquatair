import React, {useEffect, useState} from "react";
import FormCrud from "../../components/forms/Crud/FormCrud";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import APICall from "../../function/APICall";
import moment from "moment";
import css from './Gear.module.css';


function ShortIdFormater(Library, setLibrary) {
    return APICall('/category', 'GET')
        .then(res => res.json()
            .then(data => {
                    var newMap = new Map(Library)
                    for (const cat of data.data) {
                        newMap.set(cat.id, cat.attributes.shortId);
                    }
                    setLibrary(newMap);
                }
            ))
}

async function loadAssign(Assigns, setAssigns) {
    return await APICall('/affectation', 'GET')
        .then(res => res.json()
            .then(data => {
                    let array = [];
                    let count = 0;
                    for (const assign of data.data) {
                        array[count] = {
                            "id": assign.id,
                            "gearId": assign.attributes.gearId,
                            "startDate": assign.attributes.startDateAssignation,
                            "endDate": assign.attributes.endDateAssignation,
                        }
                        count++;
                    }
                    setAssigns(array);
                }
            ))
}

function Gear() {
    // Déclaration des "States" utilisés pour notre composant fonctionnel Gear (pour la page des équipements).
    const [Library, setLibrary] = useState(new Map());
    const [Assigns, setAssigns] = useState([]);

    const sortByMapped = (map, compareFn) => (a, b) => compareFn(map(a), map(b));

    const toDate = e => new Date(e).getTime();
    const byValue = (a, b) => a - b;
    const byDate = sortByMapped(toDate, byValue);

    const checkDispo = (row) => {
        const assignTri = [...Assigns].sort(byDate);
        for (const assign of assignTri) {
            if (assign.gearId === row.id) {
                if (assign.endDate !== null && assign.endDate < moment().format('L')) {
                    return (<div className={css.centerPoint}>
                        <div className={css.greenPoint}></div>
                    </div>)
                } else {
                    return (<div className={css.centerPoint}>
                        <div className={css.redPoint}></div>
                    </div>)
                }
            }
        }
    }

    const checkDispoBool = (row) => {
        const assignTri = [...Assigns].sort(byDate);
        for (const assign of assignTri) {
            if (assign.gearId === row.id) {
                if (assign.endDate !== null && assign.endDate < moment().format('L')) {
                    return "disponible - oui - en stock - in - 1"
                } else {
                    return "pas disponible - non - partis - out - 0"
                }
            }
        }
    }

    useEffect(() => {
        ShortIdFormater(Library, setLibrary);
        loadAssign(Assigns, setAssigns);
        // eslint-disable-next-line
    }, []);
    if (Library.size !== 0) {
        // Values à afficher dans le crud, c'est ici qu'on gère la façon d'affichés les données,
        // et les champs à séléctionner dans le crud
        const values = [
            {
                "dataField": "shortId",
                "text": "Référence (Simple)",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {
                            `${Library.get(row.categoryId)}_${row.shortId}`
                        }
                    </div>
                ),
                sortValue: (cell, row) => (
                    `${Library.get(row.categoryId)}_${row.shortId}`
                ),
                filterValue: (cell, row) => (
                    `${Library.get(row.categoryId)}_${row.shortId}`
                ),
            },
            {"dataField": "name", "text": "Nom", "sort": true},
            {"dataField": "mark", "text": "Marque", "sort": true},
            {
                "dataField": "id",
                "text": "Référence unique",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {
                            `${(row.id).substring(0, 8)}`
                        }
                    </div>
                ),
                sortValue: (cell, row) => (
                    `${(row.id).substring(0, 8)}`
                ),
                filterValue: (cell, row) => (
                    `${(row.id).substring(0, 8)}`
                ),
            },
            {
                "dataField": "disponibility",
                "text": "Dispo",
                "sort": true,
                formatter: (cell, row) => (
                    <div>
                        {checkDispo(row)}
                    </div>
                ),
                sortValue: (cell, row) => (
                    checkDispoBool(row)
                ),
                filterValue: (cell, row) => (
                    checkDispoBool(row)
                )
            },
        ];

        return (
            <div className="Page">
                <TopBarSimple title="Gestion des" from="équipements" type={"gear"}/>
                {/* on peux passer comme 'type' en props, le type de données qu'on veux affichés, à savoir :
                    categories
                    people
                    sites
                    gears
                    assigns
                    Toutes les infos propres aux différents affichages seront alors affichés à ce moment la */}
                <FormCrud dataInfo={values} type={"gear"}/>
            </div>
        );
    } else {
        return null
    }
}

export default Gear;