import React, {useEffect, useState} from "react";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import APICall from "../../function/APICall";
import Loader from "react-loader-spinner";
import css from "./Gear.module.css";
import Moment from 'moment';
import {Link} from "react-router-dom";

async function FetchData(setLoad, setError, idCible, setStatsData, setHiddenData) {

    let Site = [];
    let People = [];

    await APICall('/site', 'GET').then(res => res.json()).then(data => {
        Site = data.data
    })
    await APICall('/acteur', 'GET').then(res => res.json()).then(data => {
        People = data.data
    })

    return APICall('/gear/' + idCible + '/affectations', 'GET').then(res => res.json()).then(async dataAct => {

        if (dataAct.data === undefined) {
            setLoad(false);
            setError(true);
            return
        }

        let Affectations = []
        for (const el of dataAct.data) {
            await APICall('/affectation/' + el.id + '/', 'GET').then(res => res.json()).then(async dataAffect => {

                if (dataAffect.data === undefined) {
                    setLoad(false);
                    setError(true);
                    return
                }
                let done = false;
                for (const el of People) {
                    if (el.id === dataAffect.data.attributes.acteurId) {

                        dataAffect.data.attributes.acteurId = el;
                        await Affectations.push(dataAffect);
                        done = true;

                    }
                }
                if (!done) {
                    for (const el of Site) {
                        if (el.id === dataAffect.data.attributes.siteId) {

                            dataAffect.data.attributes.siteId = el;
                            await Affectations.push(dataAffect);
                        }
                    }
                }
            })
        }

        var datalist = []
        for (let aff of Affectations) {
            aff = aff.data;
            var data = {
                id: aff.id,
                startDateAssignation: new Date(aff.attributes.startDateAssignation),
                endDateAssignation: aff.attributes.endDateAssignation,
                siteId: aff.attributes.siteId,
                acteurId: aff.attributes.acteurId,
            }
            datalist.push(data);
        }
        datalist.sort((a, b) => (a.startDateAssignation > b.startDateAssignation) ? -1 : ((b.startDateAssignation > a.startDateAssignation) ? 1 : 0));
        setStatsData(datalist);
        setHiddenData(datalist);
        setLoad(false);
    })
}

function renderStatsData(statsData) {
    let result = []
    let ObjsForDate = []
    let lastEl = null
    if (statsData.length === 0) {
        return <div>Il n'y a pas de statistiques pour le moment</div>
    }
    for (const el of statsData) {
        if (lastEl !== null) {
            if (Moment(new Date(lastEl.startDateAssignation.toDateString())).isSame(new Date(el.startDateAssignation.toDateString()))) {
                ObjsForDate.push(el);
            } else {
                result.push(<DataGrid date={lastEl.startDateAssignation} objs={ObjsForDate}/>)
                ObjsForDate = [];
                ObjsForDate.push(el);
            }
        } else {
            ObjsForDate.push(el);
        }
        lastEl = el;
    }
    result.push(<DataGrid date={lastEl.startDateAssignation} objs={ObjsForDate}/>)
    return result.map(el => <>{el}</>);
}

function DateChange(date, setStatsData, hiddenData) {
    if (date.target.value === "") {
        return setStatsData(hiddenData);
    } else {
        let dateObj = new Date(date.target.value);

        let newArray = [];
        for (const data of hiddenData) {

            if (Moment(new Date(data.startDateAssignation.toDateString())).isSame(new Date(dateObj.toDateString()))) {
                newArray.push(data);
            }
        }
        return setStatsData(newArray);
    }
}


function GearStats({idCible}) {
    const [notLoad, setLoad] = useState(true);
    const [error, setError] = useState(false);
    let [statsData, setStatsData] = useState([]);
    let [hiddenData, setHiddenData] = useState([]);
    const [obj, setObj] = useState(null);
    useEffect(() => {
            FetchData(setLoad, setError, idCible, setStatsData, setHiddenData)
        }, []
    );
    useEffect(() => {
            APICall('/gear/' + idCible, 'GET').then(res => res.json()).then(data => {
                setObj(data.data);
            })
        }, []
    );
    return (
        <div className="Page">
            <TopBarSimple title="Statistique de l'" from="équipement"/>
            <div className={css.content}>
                {obj &&
                <div style={{
                    display: 'flex',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                    backgroundColor: '#009ED5',
                    height: '100px'
                }}>
                    <input type="date" onChange={(date) => {
                        DateChange(date, setStatsData, hiddenData)
                    }}/>
                    <div style={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: '#009ED5',
                        flexDirection: 'Column',
                        height: '100px'
                    }}>
                        <h2>{obj.attributes.name} {obj.attributes.lastname}</h2>
                        <h3>{obj.attributes.description}</h3>
                    </div>
                </div>
                }

                {notLoad && <Loader
                    type="TailSpin"
                    color="#043B80"
                    height={100}
                    width={100}
                />}
                {error &&
                <div className={css.errorMsg}>
                    <div className={css.error}>
                    <span className={css.errorImg}>
                        <span className={css.errorImgBar1}/>
                        <span className={css.errorImgBar2}/>
                    </span>
                        Une erreur s'est produite, essayez de réactualiser la
                        page. <br/> Si le problème persiste, contacté Andy.
                    </div>
                    <div className={`button ${css.button}`} onClick={() => {
                        window.location.reload();
                    }}> reactualiser la page
                    </div>
                </div>
                }
                {!notLoad && !error && <div className={css.dataContent}> {renderStatsData(statsData)} </div>}
            </div>
        </div>
    );
}

export default GearStats;

function DataGrid({date, objs}) {
    return (
        <div className={css.dataGrid}>
            <div className={css.dataTop}>
                <div className={css.dataTopCircle}/>
                <div className={css.dataTopTitle}>
                    {date.toLocaleDateString("fr-FR", {
                        weekday: "long",
                        year: "numeric",
                        month: "long",
                        day: "numeric"
                    })}
                </div>
            </div>
            <div className={css.dataBody}>
                <div className={css.dataBodyLine}/>
                <div className={css.dataBodyContent}>
                    {
                        objs.map(el =>
                            <Link
                                to={el.siteId !== null ? `/chantiers/${el.siteId.id}` : `/personnes/${el.acteurId.id}`}
                                className={css.dataRow}>
                                <div className={css.dataBodyContent}>
                                    <div
                                        className={css.dataRowHeader}>{el.siteId !== null ? <>Chantier
                                        : {el.siteId.attributes.name}  </> : <> Personne
                                        : {el.acteurId.attributes.firstname}</>}</div>
                                    <div className={css.dataRowBody}>
                                        <div className={css.dataRowBodyDescription}>
                                            {el.siteId !== null
                                                ? <>desciption : {el.siteId.attributes.description}  </>
                                                : <> tel : {el.acteurId.attributes.phoneNumber}</>}
                                        </div>
                                    </div>
                                </div>
                            </Link>)
                    }
                </div>
            </div>
        </div>);
}