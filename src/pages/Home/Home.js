import React from "react";
import {Link} from "react-router-dom";
import './Home.css';
import assignSvg from '../../ressources/icons/tool-box.svg';
import categorySvg from '../../ressources/icons/folderBlue.svg';
import worksiteSvg from '../../ressources/icons/constructionBlue.svg';
import userSvg from '../../ressources/icons/userBlue.svg';
import gearSvg from '../../ressources/icons/wrenchBlue.svg';
import btnAddSvg from '../../ressources/icons/btnAdd.svg';
import btnListSvg from '../../ressources/icons/btnList.svg';
import Btn from '../../components/homePageButton/Btn';
import background from '../../ressources/icons/aquatair.svg'


class Home extends React.Component {
    render() {
        return (
            <div className="Page">
                <div className="home-page">
                    <div className="home-page-element actor">
                        <h2>Personnes</h2>
                        <Link to="/personnes">
                            <img src={userSvg} alt="userBlue.svg"/>
                        </Link>
                        <div className="home-page-button">
                            <Btn path="/personnes-ajouter" svg={btnAddSvg} text="Ajouter"
                                 buttonType="button button-add"/>
                            <Btn path="/personnes" svg={btnListSvg} text="Voir liste" buttonType="button button-list"/>
                        </div>
                    </div>
                    <div className="home-page-element site">
                        <h2>Chantiers</h2>
                        <Link to="/chantiers">
                            <img src={worksiteSvg} alt="constructionBlue.svg"/>
                        </Link>
                        <div className="home-page-button">
                            <Btn path="/chantiers-ajouter" svg={btnAddSvg} text="Ajouter"
                                 buttonType="button button-add"/>
                            <Btn path="/chantiers" svg={btnListSvg} text="Voir liste" buttonType="button button-list"/>
                        </div>
                    </div>
                    <div className="home-page-element assign">
                        <h2>Affectations</h2>
                        <Link to="/affectations">
                            <img src={assignSvg} alt="tool-box.svg"/>
                        </Link>
                        <div className="home-page-button">
                            <Btn path="/affectations-ajouter" svg={btnAddSvg} text="Ajouter"
                                 buttonType="button button-add"/>
                            <Btn path="/affectations" svg={btnListSvg} text="Voir liste"
                                 buttonType="button button-list"/>
                        </div>
                    </div>
                    <div className="home-page-element category">
                        <h2>Catégories</h2>
                        <Link to="/categories">
                            <img src={categorySvg} alt="folderBlue.svg"/>
                        </Link>
                        <div className="home-page-button">
                            <Btn path="/categories-ajouter" svg={btnAddSvg} text="Ajouter"
                                 buttonType="button button-add"/>
                            <Btn path="/categories" svg={btnListSvg} text="Voir liste" buttonType="button button-list"/>
                        </div>
                    </div>
                    <div className="home-page-element gear">
                        <h2>Équipements</h2>
                        <Link to="/equipements">
                            <img src={gearSvg} alt="wrenchBlue.svg"/>
                        </Link>
                        <div className="home-page-button">
                            <Btn path="/equipements-ajouter" svg={btnAddSvg} text="Ajouter"
                                 buttonType="button button-add"/>
                            <Btn path="/equipements" svg={btnListSvg} text="Voir liste"
                                 buttonType="button button-list"/>
                        </div>
                    </div>
                    <div className={"home-page-background"}>
                        <img src={background} alt="backgroundaquatair"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;