import React from "react";
import './Login.css';
import APICall from "../../function/APICall";
import Cookies from 'js-cookie';
import aquataire from '../../ressources/icons/aquatair.svg';
import Loader from 'react-loader-spinner';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: Cookies.get('username'),
            password: '',
            showInput: true,
            error: false,
            error_msg: "test"
        };
        this.handleChangeU = this.handleChangeUsername.bind(this);
        this.handleChangeP = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChangeUsername(event) {
        this.setState({username: event.target.value});

    }

    handleChangePassword(event) {
        this.setState({password: event.target.value});

    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({showInput: false})
        APICall('/user/auth?username=' + this.state.username + '&password=' + this.state.password, 'GET').then(res => res.json()).then(data => {
            if (data.meta.result.token !== undefined) {
                Cookies.set('username', this.state.username, {
                    expires: 365
                });
                Cookies.set('token', data.meta.result.token, {
                    expires: 1 / 48
                });
                window.location = '/';
            }
            if (data.meta.result.error !== undefined) {
                this.setState({
                    error: true,
                    error_msg: data.meta.result.error
                })
                this.setState({showInput: true})
            }
        })
    }

    renderForm() {
        return (
            <form className="login-form" onSubmit={this.handleSubmit}>
                <label className="login-label" htmlFor="username">Identifiant</label>
                <input className="login-input" type="text" name="username" value={this.state.username}
                       onChange={this.handleChangeU}/>

                <label className="login-label" htmlFor="password">Mot de passe</label>
                <input className="login-input" type="password" name="password" onChange={this.handleChangeP}/>

                <div className="login-buttons">
                    <input className="login-button" type="submit" value="Envoyer ⯈"/>
                </div>
            </form>
        )
    }

    render() {
        return (
            <div className="login-page">
                <div className="login-container">

                    <div className="login-top">
                        <img class="login-logo" src={aquataire} alt=""/>
                        <h1 className="login-h1">Connexion</h1>
                    </div>
                    {this.state.showInput && this.renderForm()}
                    {!this.state.showInput && <Loader
                        type="TailSpin"
                        color="#043B80"
                        height={100}
                        width={100}
                    />}
                    {this.state.error &&
                    <div className="login-alert">
                        <div className="container">
                            {this.state.error_msg}
                        </div>
                    </div>
                    }
                </div>
            </div>
        );
    }
}

export default Login;