import React from "react";
import notfoundsvg from '../ressources/icons/404.svg';
import './NotFound.css'

function NotFound() {
    return (
        <div className="Page">
            <div className={"notFound"}>
                <img className={"notFound"} src={notfoundsvg} alt="Page non trouver - erreur 404"/>
            </div>
        </div>
    );
}

export default NotFound;
