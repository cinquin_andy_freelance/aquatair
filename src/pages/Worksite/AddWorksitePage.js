import React from "react";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";
import AddWorksiteForm from "../../components/forms/Worksite/AddWorksiteForm";


// Composant correspondant à la page d'ajout des chantiers.
class AddWorksitePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="Page">
                <TopBarSimple title="Ajout d'un nouveau" from="chantier"/>
                {/*Appel du composant formulaire pour l'ajout de chantier*/}
                <AddWorksiteForm/>
            </div>
        );
    };
}

export default AddWorksitePage;