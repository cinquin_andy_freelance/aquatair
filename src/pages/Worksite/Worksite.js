import React from "react";
import FormCrud from "../../components/forms/Crud/FormCrud";
import TopBarSimple from "../../components/topBarSimple/TopBarSimple";


function Worksite() {
    // Values à afficher dans le crud, c'est ici qu'on gère la façon d'affichés les données,
    // et les champs à séléctionner dans le crud
    const values = [
        {"dataField": "name", "text": "Nom", "sort": true},
        {"dataField": "city", "text": "Ville", "sort": true},
        {"dataField": "address", "text": "Adresse", "sort": true}
    ];

    const defaultSorted = [{
        dataField: "city",
        order: "asc"
    }];

    return (
        <div className="Page">
            <TopBarSimple title="Gestion des" from="chantiers" type={"site"}/>
            {/* on peux passer comme 'type' en props, le type de données qu'on veux affichés, à savoir :
                    category
                    acteur
                    site
                    gear
                    affectation
                    Toutes les infos propres aux différents affichages seront alors affichés à ce moment la */}
            <FormCrud dataInfo={values} type={"site"} defaultSort={defaultSorted}/>
        </div>
    );
}

export default Worksite;